Pod::Spec.new do |s|
    s.name         = "HelperModule"
    s.version      = "0.0.1"
    s.summary      = "This is an helper module that contain's many files that can help any other module"
    s.homepage     = "https://gitlab.com/aminfaruq/base_project"
    s.license      = "MIT (ios)"
    s.author             = { "Amin Faruq" => "aminfaruk.fa@gmail.com" }
    s.source       = { :git => "https://gitlab.com/aminfaruq/base_project.git", :tag => "#{s.version}" }
    s.source_files  = "HelperModule/Classes/**/*.{h,m,swift,a}"
    s.resource_bundles = {'HelperModule' => ['HelperModule/Assets/**/*.{storyboard,xib,xcassets,json,imageset,png,bundle,ttf,html}']}
    s.platform         = :ios, "10.0"
    s.dependency 'ChameleonFramework'
    s.dependency 'FittedSheets', '~> 1.4.6'
    s.dependency 'IQKeyboardManager'
    s.dependency 'IGListKit'
    s.dependency 'JGProgressHUD'
    s.dependency 'Material'
    s.dependency 'M13Checkbox'
    s.dependency 'PureLayout', '3.1.7'
    s.dependency 'SDWebImage'
    s.dependency 'SDWebImage/GIF'
    s.dependency 'Shimmer'
    s.dependency 'SwiftMessages'
    s.dependency 'UIColor_Hex_Swift'
    s.dependency 'ZSWTappableLabel'
    s.dependency 'ZSWTaggedString/Swift'
    s.dependency 'DropDown', '2.3.13'
    s.dependency 'TOCropViewController'

end
