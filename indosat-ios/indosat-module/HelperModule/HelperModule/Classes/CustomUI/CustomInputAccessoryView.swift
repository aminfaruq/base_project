//
//  CustomInputAccessoryView.swift
//  HelperModule
//
//  Created by GMV on 28/01/21.
//  Copyright © 2021 GITS Indonesia. All rights reserved.
//

import UIKit

public class CustomInputAccessoryView: BaseCustomView {
    // MARK: Properties
    private var contentView: UIView!
    public var button: CustomButton!
    
    // MARK: Function
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.contentView = UIView(frame: .zero)
        self.addSubview(self.contentView)
        
        self.button = CustomButton(frame: .zero)
        self.button.applyStyleFont(title: "Lanjutkan", font: UIFont.Poppins.bold.font(ofSize: 14), fontColor: .white)
        self.contentView.addSubview(self.button)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
            self.button.applyStyleBackground(backgroundColor: UIColor("#14725D"), isGradientTopBottom: true, gradientColorTop: UIColor("#14725D"), gradientColorBottom: UIColor("#19A788"))
            self.button.applyStyleBorder(shadow: 0, borderColor: .none, borderWidth: 0, borderRadius: 0)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func initConstraint() {
        self.contentView.autoPinEdgesToSuperviewEdges()
        self.button.autoPinEdgesToSuperviewEdges(with: .zero)
    }
}


extension CustomInputAccessoryView {
    public func setTitle(_ title: String) {
        self.button.applyStyleFont(title: title, font: UIFont.Poppins.bold.font(ofSize: 14), fontColor: .white)
    }
    public static func `default`()-> CGRect {
        return CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40)
    }
}
