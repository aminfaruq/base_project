//
//  CustomLoadMoreDataSource.swift
//  HelperModule
//
//  Created by Irfan Handoko on 19/04/21.
//

import UIKit

public class LoadMoreSectionController: BaseListSectionController {
    
    public var isLoad: Bool = false
    // MARK: Function
    /** Initialization
     */
    public override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.minimumInteritemSpacing = 16
        self.minimumLineSpacing = 16
        self.onRefreshDataSection = {
            self.collectionContext?.performBatch(animated: true, updates: { batchContext in
              batchContext.reload(self)
            }, completion: nil)
        }
    }
    
    /** Number of items in one section
     */
    public override func numberOfItems() -> Int {
        return self.isLoad ? 1 : 0
    }
    
    /** Set the cell here
    */
    public override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = LoadMoreCell.initCell(self.collectionContext, section: self, index: index, item: self.isLoad) as! LoadMoreCell
        return cell
    }
    
    /** Size of the item
    */
    public override func sizeForItem(at index: Int) -> CGSize {
        return LoadMoreCell.cellSize(screenWidth: self.collectionContext?.containerSize.width ?? 0, item: self.isLoad)
    }
}

