//
//  CustomLoadMoreCell.swift
//  HelperModule
//
//  Created by Irfan Handoko on 19/04/21.
//


import UIKit

public class LoadMoreCell: BaseCollectionCell {
    // MARK: Properties
    public var loadingVw: UIActivityIndicatorView!
    
    /// Item
    public override var item: Any? {
        didSet {
            guard let isLoad = item as? Bool else { return }
            if isLoad {
                DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
                    self.loadingVw.startAnimating()
                }
            } else {
                self.loadingVw.stopAnimating()
            }
        }
    }
    
    // MARK: Function
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configLoader()
        self.updateConstraints()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /** Initialization the constraint
    */
    public override func initConstraint() {
        self.loadingVw.autoAlignAxis(.horizontal, toSameAxisOf: self.contentView)
        self.loadingVw.autoAlignAxis(.vertical, toSameAxisOf: self.contentView)
    }
    
    public override class func cellSize(screenWidth: CGFloat, item: Any? = nil) -> CGSize {
        return CGSize(width: screenWidth, height: 40)
    }
}

extension LoadMoreCell {
    
    func configLoader() {
        self.backgroundColor = .white
        self.loadingVw = UIActivityIndicatorView(frame: .zero)
        self.loadingVw.style = .whiteLarge
        self.loadingVw.color = .gray
        self.loadingVw.contentMode = .center
        self.contentView.addSubview(self.loadingVw)
    }
    
}
