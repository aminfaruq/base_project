//
//  BaseCustomView.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 26/09/19.
//

import UIKit
import PureLayout

/// A Class of Base Custom View
open class BaseCustomView: UIView {
    // MARK: Properties
    /// Setup constraint
    public var shouldSetupConstraints = true
    /// A name of a view
    private static var name: String {
        let type = String(describing: self)
        return type
    }
    /// Override this Item to set your item to your view
    open var item: Any?
    
    public weak var controller: UIViewController?
    
    // MARK: Function
    /**
     */
    public class func instantiateViewFromNib(item: Any?) -> BaseCustomView {
        let view = UINib(nibName: name, bundle: HelperBundle.bundle).instantiate(withOwner: nil, options: [:])[0] as! BaseCustomView
        view.item = item
        return view
    }
    
    public func instantiate(controller from: UIViewController) {
        controller = from
        instantiateView(from: from.view)
    }
    
    public func instantiateView(from view: UIView) {
        view.addSubview(self)
        self.autoPinEdgesToSuperviewEdges(with: .zero)
    }
    
    public func openDialog(at superview: UIView) {
        superview.addSubview(self)
        self.autoPinEdgesToSuperviewEdges(with: .zero)
        self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = 1.0
            self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        })
    }
    
    /** Update Constraints
     */
    open override func updateConstraints() {
        if shouldSetupConstraints {
            self.initConstraint()
            // Auto Layout Constraints
            shouldSetupConstraints = false
        }
        super.updateConstraints()
    }
    
    /** Initialization the constraint
     */
    open func initConstraint() {
        // Override this to init your constraint
    }
    
    /** Set Button Handler
     */
    open func setButtonHandler(handler: ((UIView)->())?) {
        // Override this class to set the button action
    }
    
    /** Set Bar Button Item Handler
     */
    open func setBarButtonItemHandler(handler: ((UIBarButtonItem)->())?) {
        // Override this class to set the bar button item action
    }
}
