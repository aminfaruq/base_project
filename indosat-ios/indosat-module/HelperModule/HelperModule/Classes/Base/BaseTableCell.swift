//
//  BaseTableCell.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 26/09/19.
//

import UIKit

open class BaseTableCell: UITableViewCell {
    // MARK: Properties
    /// Setup constraint
    public var shouldSetupConstraints = true
    /// A name of a view
    private static var name: String {
        let type = String(describing: self)
        return type
    }
    /// Override this Item to set your item to your view
    public var item: Any?
    
    // MARK: Function
    /// Register a cell
    public static func registerCell(with tableVw: UITableView, bundle: Bundle?) {
        tableVw.register(UINib(nibName: name, bundle: bundle), forCellReuseIdentifier: name)
    }
    
    /// init a cell
    public static func initCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, item: Any?) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: name, for: indexPath) as? BaseTableCell else { return UITableViewCell() }
        cell.item = item
        cell.selectionStyle = .none
        return cell
    }
}
