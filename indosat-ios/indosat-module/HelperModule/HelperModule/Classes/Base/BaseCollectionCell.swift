//
//  BaseCollectionCell.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 28/01/20.
//

import UIKit
import IGListKit

open class BaseCollectionCell: UICollectionViewCell {
    // MARK: Properties
    /// IndexPath of Collection Cell
    public var index: Int!
    /// Setup constraint
    public var shouldSetupConstraints = true
    /// A name of a view
    private static var name: String {
        let type = String(describing: self)
        return type
    }
    /// Override this Item to set your item to your view
    open var item: Any?
    
    // MARK: Function
    /** Register a cell collection to ui collection view
     */
    public static func registerCell(with collectionView: UICollectionView, bundle: Bundle?) {
        let nib = UINib(nibName: name, bundle: bundle)
        collectionView.register(nib, forCellWithReuseIdentifier: name)
    }
    
    /** Initialization a cell collection using Pure Layout
     */
    public static func initCell(_ collectionContext: ListCollectionContext?,
                                section: ListSectionController,
                                index: Int,
                                item: Any? = nil) -> UICollectionViewCell {
        guard let collectionContext = collectionContext,
              let cell = collectionContext.dequeueReusableCell(of: self, withReuseIdentifier: name, for: section, at: index) as? BaseCollectionCell else {
            return UICollectionViewCell()
        }
        
        cell.item = item
        cell.index = index
        
        return cell
    }
    
    /** Initialization a cell collection using XIB
     */
    public static func initCell(_ collectionContext: ListCollectionContext?, section: ListSectionController, index: Int, bundle: Bundle?, item: Any? = nil) -> UICollectionViewCell {
        guard let collectionContext = collectionContext, let cell = collectionContext.dequeueReusableCell(withNibName: self.name, bundle: bundle, for: section, at: index) as? BaseCollectionCell else {
            return UICollectionViewCell()
        }
        cell.index = index
        cell.item = item
        return cell
    }
    
    
    /** Update Constraint
     */
    open override func updateConstraints() {
        if shouldSetupConstraints {
            self.initConstraint()
            // Auto Layout Constraints
            shouldSetupConstraints = false
        }
        super.updateConstraints()
    }
    
    /** Initialization the constraint
     */
    open func initConstraint() {
        // Override this to init your constraint
    }
    
    /** Content Cell Size
     */
    open class func cellSize(screenWidth: CGFloat, item: Any? = nil) -> CGSize {
        return .zero
    }
    
    /** Set Button Handler
     */
    open func setButtonHandler(handler: ((UIView)->())?) {
        // Override this class to set the button action
    }
}
