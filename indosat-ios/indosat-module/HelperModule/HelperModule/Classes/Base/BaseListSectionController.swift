//
//  BaseListSectionController.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 16/11/20.
//  Copyright © 2020 GITS Indonesia. All rights reserved.
//

import UIKit
import IGListKit


open class BaseListSectionController: ListSectionController {
    /// A Closure for refresh data section
    public var onRefreshDataSection: (()->())?
    /// A Closure for selected data
    public var onSelectedItem: ((_ section: Int, _ idx: Int)->())?
}
