//
//  BaseVC.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 28/08/19.
//  Copyright © 2019 GITS Indonesia. All rights reserved.
//

import UIKit
import Material
import PureLayout
import IGListKit

open class BaseVC: UIViewController {
    private var refreshUIControl: UIRefreshControl?
    private var refreshEmptyUIControl: UIRefreshControl?
    public var stillHasFocused = false
    private lazy var backImage = UIImage.load(image: "ic_back")
    public var emptyVw: EmptyView?
    /// Load More
    public var isLoadMore: Bool?
    public var isLoadMoreApi = false
//    public var isScrolling = falseb
    public var onReachBottomAction: (()->())?
    
    deinit {
        print("DEINIT \(self)")
    }
    
    open override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(becomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(enterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    @objc open func becomeActive() {
        // Override this method to get a notify when the app is become active
    }
    
    @objc open func enterBackground() {
        // Override this method to get a notify when the app is become active
    }
    
    @objc open func rotated() {
        // Override this method to get a notify when the phone got rotated
    }
    
    @objc open func keyboardWillShow(notification: NSNotification){
        // Override this method to get a notify when keyboard will show
    }
    
    @objc open func keyboardWillHide(notification: NSNotification){
        // Override this method to get a notify when keyboard will hide
    }
    
    open func configureData(data: [String: Any?]){
        // Override this method to configure data from another page
    }
    
    open func configureView(){
        // Override this method to Configure the View
    }
    
    open func loadMore() {
        // Override this func to get a notify from load more the scrollView
    }
    
    open func checkInputValidator(showError: Bool = false, to textField: ErrorTextField? = nil)-> Bool {
        return true
    }
    
    public func updateBackBtn() {
        navigationController?.configColorBar(
            colorBar: [.black ],
            colorTitle: .black,
            colorBarButton: .black,
            alpha: 0,
            isTranslucent: true
        )
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage,
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(actionBack))
    }
    
    @objc open func actionBack() {
        navigationController?.popViewController(animated: true)
    }
    
    open func setupEmptyView(view: UIView, isHide: Bool, title: String, desc: String, image: String) {
        if !isHide && self.emptyVw == nil {
            self.emptyVw = EmptyView()
            self.emptyVw?.instantiateView(from: view)
            self.emptyVw?.setup(title: title, desc: desc, image: image)
            self.refreshEmptyUIControl = UIRefreshControl()
            self.refreshEmptyUIControl?.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
            self.emptyVw?.scrollView.refreshControl = self.refreshEmptyUIControl
        } else if isHide {
            self.refreshEmptyUIControl?.endRefreshing()
            self.refreshEmptyUIControl?.removeTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
            self.emptyVw?.scrollView.refreshControl = nil
            self.refreshEmptyUIControl = nil
            self.emptyVw?.removeFromSuperview()
            self.emptyVw = nil
        }
    }
}

extension BaseVC {
    open func initRefresh(on view: UIScrollView) {
        self.refreshUIControl = UIRefreshControl()
        self.refreshUIControl?.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        view.refreshControl = refreshUIControl
    }
    
    open func removeRefresh(from view: UIScrollView) {
        self.refreshUIControl?.endRefreshing()
        self.refreshUIControl?.removeTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        view.refreshControl = nil
        self.refreshUIControl = nil
    }
    
    open func stopRefresh() {
        self.refreshUIControl?.endRefreshing()
        self.refreshEmptyUIControl?.endRefreshing()
    }
    
    @objc open func refresh() {
        // Override this func to get a notify from refreshing the scrollView
    }
    
    open func changeView(from current: UIView, to replacement: UIView){
        current.removeFromSuperview()
        self.view.addSubview(replacement)
        replacement.autoPinEdgesToSuperviewEdges()
    }
}

extension BaseVC: UIScrollViewDelegate {
    
    open func initLoadMore(on adapter: ListAdapter) {
        adapter.scrollViewDelegate = self
    }
    
//    open func scrollViewWillEndDragging(_ scrollView: UIScrollView,
//                                   withVelocity velocity: CGPoint,
//                                   targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//        let distance = scrollView.contentSize.height - (targetContentOffset.pointee.y + scrollView.bounds.height)
//        if !self.isScrolling && distance < 200 {
//            debugPrint("Load Start")
//            self.isScrolling = true
//            DispatchQueue.global(qos: .default).async {
//                // fake background isScrolling task
//                sleep(1)
//                DispatchQueue.main.async {
//                    debugPrint("Load Stop")
//                    self.isScrolling = false
//                    if self.isLoadMore ?? false {
//                        self.loadMore()
//                    }
//                }
//            }
//        }
//    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset: CGFloat = 0
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if (bottomEdge + offset >= scrollView.contentSize.height && (self.isLoadMore ?? false) && !self.isLoadMoreApi) {
            // Load next batch of products
            self.onReachBottomAction?()
        }

    }
}
