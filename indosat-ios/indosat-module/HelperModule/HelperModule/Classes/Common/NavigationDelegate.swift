//
//  NavigationDelegate.swift
//  HelperModule
//
//  Created by Ajie Pramono on 25/11/20.
//  Copyright © 2020 GITS Indonesia. All rights reserved.
//

import UIKit
import CoreLocation

public protocol NavigationDelegate {
    func initToSample()-> UIViewController
}

public protocol NavigationOutputDelegate {
    
}

public class NavigationManager {
    public static var shared = NavigationManager()
    public var navDelegate: NavigationDelegate?
}
