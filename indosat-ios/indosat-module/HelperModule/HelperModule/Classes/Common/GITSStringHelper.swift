//
//  StringHelper.swift
//  GITSFramework
//
//  Created by GITS INDONESIA on 11/24/17.
//  Copyright © 2017 GITS Indonesia. All rights reserved.
//

import UIKit

public class GITSStringHelper {
    public static func generateDeviceID()->String{
        //device id
        let deviceid = UIDevice.current.identifierForVendor?.uuidString
        return deviceid!
    }
    
    public static func currencyFormatting(value: Double, isUsingDecimal: Bool = false, digit: Int = 0) -> String {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = isUsingDecimal ? digit : 0
        formatter.minimumFractionDigits = isUsingDecimal ? digit : 0
        formatter.currencySymbol = "Rp "
        formatter.locale = Locale(identifier: "ID_id")
        formatter.numberStyle = .currencyAccounting
        return formatter.string(from: value as NSNumber) ?? "Rp 0"
    }
    
    public static func replaceFirstZero(string: String)-> String {
        var isFirstZero = true
        var newStr: String = ""
        for char in string {
            if char == "0" && isFirstZero {
                continue
            }
            else {
                isFirstZero = false
                newStr.append(char)
            }
        }
        return newStr
    }
    
    public static func isValidPassword(text: String) -> (Bool, Bool) {
//        let capitalLetterRegEx  = ".*[A-Z]+.*"
//        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
//        let capitalresult = texttest.evaluate(with: text)
//
//        let lowerLetterRegEx  = ".*[a-z]+.*"
//        let texttest2 = NSPredicate(format:"SELF MATCHES %@", lowerLetterRegEx)
//        let lowerresult = texttest2.evaluate(with: text)
        
        let letters = NSCharacterSet.letters
        let rangeLetter = text.rangeOfCharacter(from: letters)
        
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: text)
        
        return (rangeLetter != nil, numberresult)
    }
    
    static func isTextNotEmpty(text: String, priceInput: Bool = false)-> Bool {
        var textValue = text
        textValue = priceInput ? textValue.replacingOccurrences(of: "Rp ", with: "").replacingOccurrences(of: ".", with: "") : textValue
        if priceInput {
            return textValue != "0"
        } else {
            return !(textValue.isEmpty)
        }
    }
     
    public static func isLengthTextMinMax(text: String, min: Int, max: Int, isPhoneNum: Bool = false)-> Bool {
        let textValue = isPhoneNum ? text.replacingOccurrences(of: " ", with: "") : text
        return textValue.count >= min && (max == 0 || textValue.count <= max)
    }
    
    public static func validatePhoneNum(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result = phoneTest.evaluate(with: value)
        return result
    }
     
    static func compareText(text: String, withText: String)-> Bool {
        return text == withText
    }
    
    public static func isTextEmailRegex(text: String)->Bool {
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}" +
                 "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
                 "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-" +
                 "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
                 "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
                 "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
             "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
    
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: text)
    }
     
    static func textMaxInput(text: String?, max: Int, range: NSRange, newString: String, isPhoneNum: Bool = false)-> Bool {
        guard let textFieldText = text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
            return false
        }
         
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = (isPhoneNum ? textFieldText.replacingOccurrences(of: " ", with: "").count : textFieldText.count) - (isPhoneNum ? substringToReplace.replacingOccurrences(of: " ", with: "").count : substringToReplace.count) + (isPhoneNum ? newString.replacingOccurrences(of: " ", with: "").count : newString.count)
        return count <= max
    }
     
    static func textRegexOnlyNum(newString: String, isPhoneNum: Bool = false, range: NSRange)-> Bool {
        let newString = isPhoneNum ? newString.replacingOccurrences(of: " ", with: "") : newString
        let numberRegEx  = "[0-9]+"
        let textRegex = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        if isPhoneNum && range.location == 0 && newString == "0" {
            return false
        } else {
            return newString == "" || textRegex.evaluate(with: newString)
        }
    }
    
    public static func localized(key: String, bundle: Bundle) -> String {
        return NSLocalizedString(key, tableName: nil, bundle: bundle, value: "", comment: "")
    }
    
    public static func convertDictionaryToJSONString(data: [String: Any]) -> String{
        if let data = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted),
            let str = String(data: data, encoding: .utf8) {
            return str
        }
        return ""
    }
    
    public static func convertJSONStringToDictionary(text: String) -> [String: Any] {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] ?? [:]
            } catch {
                print(error.localizedDescription)
            }
        }
        return [:]
    }
}

extension String {
    public func localized(lang: String)-> String {
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path ?? "")
        return NSLocalizedString(self, tableName: nil, bundle: bundle ?? Bundle(), value: "", comment: "")
    }
    
    public func htmlLinkDetector() -> String {
        var resultText = self
        let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector?.matches(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count))
        
        for match in matches ?? [] {
            guard let range = Range(match.range, in: self) else { continue }
            let url = self[range]
            resultText = resultText.replacingOccurrences(of: url, with: "<a href=\"\(url)\">\(url)</a>", options: .literal, range: nil)
        }
        return resultText
    }
    
    public var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
    public var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    public func formatDouble(digit: Int = 2) -> String {
        return String(format: "%.\(digit)f", Double(self) ?? 0)
    }
}
