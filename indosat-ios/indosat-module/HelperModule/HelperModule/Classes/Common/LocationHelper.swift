//
//  LocationHelper.swift
//
//
//  Created by Ajie Pramono Arganata on 18/09/19.
//

import UIKit
import CoreLocation

public class LocationHelper: NSObject {
    public static let instance = LocationHelper()
    public var onGetLocation: ((_ latitude: Double?, _ longitude: Double?, _ isFailed: Bool)->())?
    
    private var isStartUpdated = false
    private let locationManager = CLLocationManager()
    
    public func getLocationPermission(fromVC: UIViewController, onCancelDialog: (()->())?) {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            switch CLLocationManager.authorizationStatus() {
            case .denied:
                self.showDeniedDialog(fromVC: fromVC, onCancelDialog: onCancelDialog)
                self.isStartUpdated = false
                self.onGetLocation?(nil, nil, true)
            case .authorizedWhenInUse:
                self.isStartUpdated = true
                self.locationManager.startUpdatingLocation()
            default:
                break
            }
        } else {
            self.showDeniedDialog(fromVC: fromVC, onCancelDialog: onCancelDialog)
        }
    }
    
    private func showDeniedDialog(fromVC: UIViewController, onCancelDialog: (()->())?) {
        let alert = UIAlertController(title: "Permission Dialog", message: "Izin lokasi Anda ditolak atau layanan Anda dinonaktifkan, Anda harus mengubahnya pada menu pengaturan", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Tutup", style: .cancel, handler: { (action) in
            self.locationManager.stopUpdatingLocation()
            self.locationManager.delegate = nil
            self.onGetLocation?(nil, nil, true)
            onCancelDialog?()
        }))
        alert.addAction(UIAlertAction(title: "Buka Pengaturan", style: .default, handler: { (action) in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl)
            }
        }))
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            fromVC.present(alert, animated: true, completion: nil)
        })
    }
}

extension LocationHelper: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse:
            self.isStartUpdated = true
            manager.startUpdatingLocation()
        case .denied:
            self.isStartUpdated = false
            self.onGetLocation?(nil, nil, true)
        default:
            break
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let lastLocation = locations.last, isStartUpdated else { return }
        self.onGetLocation?(lastLocation.coordinate.latitude, lastLocation.coordinate.longitude, false)
        self.isStartUpdated = false
        manager.stopUpdatingLocation()
        manager.delegate = nil
    }
}
