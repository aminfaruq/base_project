//
//  HelperVC.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 26/10/20.
//  Copyright © 2020 GITS Indonesia. All rights reserved.
//

import UIKit
import JGProgressHUD
import SwiftMessages

public class HelperVC {
    private static var loadingVw: JGProgressHUD?
    
    public static func initRootViewController(vc: UIViewController)-> UIWindow {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = UIColor.white
        window.rootViewController = vc
        window.makeKeyAndVisible()
        return window
    }
    
    public static func changeRootViewController(vc: UIViewController) {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        let trans = CATransition()
        trans.type = CATransitionType.push
        trans.subtype = CATransitionSubtype.fromTop
        window.layer.add(trans, forKey: kCATransition)
        window.rootViewController = vc
        window.makeKeyAndVisible()
    }
    
    public static func loadNetworkActivityIndicator(visible: Bool){
        UIApplication.shared.isNetworkActivityIndicatorVisible = visible
    }
    
    public static func showLoading(view: UIView, isLoad: Bool) {
        if isLoad {
            loadingVw?.dismiss()
            loadingVw = nil
            loadingVw = JGProgressHUD()
            loadingVw?.style = .dark
            loadingVw?.show(in: view, animated: true)
        } else {
            loadingVw?.dismiss()
            loadingVw = nil
        }
    }
    
    public enum MessageOptionKey {
        case interface
        case backgroundColor
        case textColor
        case layout
        case style
    }
    
    public static func showMessage(icon: String? = nil, title: String? = nil, body: String?, state: Bool = true, option: [MessageOptionKey: Any]) {
        let layout = option[.layout] as? MessageView.Layout ?? .messageView
        let style = option[.style] as? SwiftMessages.PresentationStyle ?? .top
        var backgroundColor = option[.backgroundColor] as? UIColor ?? .darkGray
        var textColor = option[.textColor] as? UIColor ?? .white
        if #available(iOS 12.0, *), let interfaceStyle = option[.interface] as? UIUserInterfaceStyle {
            backgroundColor = option[.backgroundColor] as? UIColor ?? (interfaceStyle == .light ? .darkGray : .white)
            textColor = option[.textColor] as? UIColor ?? (interfaceStyle == .light ? .white : .darkGray)
        }
        
        let messageView = MessageView.viewFromNib(layout: layout)
        messageView.configureContent(title: title, body: body, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "", buttonTapHandler: { _ in SwiftMessages.hide() })
        let image = UIImage.loadLocalImage(bundle: HelperBundle.bundle, moduleName: HelperBundle.moduleName, name: icon ?? "", ext: "png")
        messageView.configureTheme(backgroundColor: backgroundColor, foregroundColor: textColor, iconImage: image, iconText: "")
        // Component
        messageView.button?.isHidden = true
        messageView.titleLabel?.isHidden = title?.isEmpty ?? false
        // Config
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        config.duration = .seconds(seconds: 5)
        config.preferredStatusBarStyle = .lightContent
        config.presentationStyle = style
        // Show and Hide
        if state {
            SwiftMessages.show(config: config, view: messageView)
        } else {
            SwiftMessages.hide()
        }
    }
}
