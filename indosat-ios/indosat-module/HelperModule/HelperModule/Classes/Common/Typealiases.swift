//
//  Typealiases.swift
//  HelperModule
//
//  Created by IHsan HUsnul on 20/02/21.
//

import Foundation

public typealias VoidClosure = (() -> Void)
