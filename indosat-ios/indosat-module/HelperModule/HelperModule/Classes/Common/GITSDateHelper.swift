//
//  DateHelper.swift
//  GITSFramework
//
//  Created by GITS Indonesia on 3/14/17.
//  Copyright © 2017 GITS Indonesia. All rights reserved.
//

import Foundation

/// Helper for manage date format
public class GITSDateHelper {
    
    /**
       Call this function to get date format
       - Parameters:
           - format: original date format.
           - localeId: locale with the specified identifier.
           - timezoneId: values represent geopolitical regions.
    
    ### Usage Examples: ###
    ```
    let dateFormatter = getDateFormatter(localeId: localeId, timezoneId: timezoneId, format: format)
    ```
    */
    public static func getDateFormatter(localeId: String, timezoneId: String, format: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = localeId.isEmpty ? .none : Locale(identifier: localeId)
        dateFormatter.timeZone = timezoneId.isEmpty ? .none : TimeZone(identifier: timezoneId)
        return dateFormatter
    }
    
    /**
        Call this function to change date format
        - Parameters:
            - dateStr: original date string.
            - format: original date format.
            - newFormat: new date format.
            - localeId: locale with the specified identifier.
            - timezoneId: values represent geopolitical regions.
     
     ### Usage Examples: ###
     ```
     let date = GITSDateHelper.changeStringFormat(dateStr: "2020-02-10 12:07:12+0700", format: "yyyy-MM-dd HH:mm:ss", newFormat: "dd MMM yyyy", localeId: "", timezoneId: "id")
     ```
     */
    public static func changeStringFormat(dateStr: String, format: String, newFormat: String, localeId: String, timezoneId: String) -> String {
        let dateFormatter = getDateFormatter(localeId: localeId, timezoneId: timezoneId, format: format)
        if let newDateStr = dateFormatter.date(from: dateStr) {
            dateFormatter.dateFormat = newFormat
            return dateFormatter.string(from: newDateStr)
        } else {
            return ""
        }
    }
    
    /**
       Call this function to convert date from date to string
       - Parameters:
           - date: original date.
           - format: original date format.
           - localeId: locale with the specified identifier.
           - timezoneId: values represent geopolitical regions.
    
    ### Usage Examples: ###
    ```
    let date = GITSDateHelper.dateToString(date: Date(), format: "yyyy-MM-dd HH:mm:ssZ", localeId: "", timezoneId: "id")
    ```
    */
    public static func dateToString(date: Date, format: String, localeId: String, timezoneId: String) -> String{
        let dateFormatter = getDateFormatter(localeId: localeId, timezoneId: timezoneId, format: format)
        return dateFormatter.string(from: date)
    }
    
    /**
       Call this function to convert date from string to date
       - Parameters:
           - dateStr: original date.
           - format: original date format.
           - localeId: locale with the specified identifier.
           - timezoneId: values represent geopolitical regions.
    
    ### Usage Examples: ###
    ```
    let date = GITSDateHelper.stringToDate(date: "2020-02-10 12:07:12+0700", format: "yyyy-MM-dd HH:mm:ssZ", localeId: "", timezoneId: "id")
    ```
    */
    public static func stringToDate(dateStr: String, format: String, localeId: String, timezoneId: String) -> Date {
        let dateFormatter = getDateFormatter(localeId: localeId, timezoneId: timezoneId, format: format)
        return dateFormatter.date(from: dateStr) ?? Date()
    }
    
    /**
       Call this function to add date by several months
       - Parameters:
           - dateStr: original date.
           - format: original date format.
           - localeId: locale with the specified identifier.
           - timezoneId: values represent geopolitical regions.
           - value: months number to added.
     
    ### Usage Examples: ###
    ```
    let date = GITSDateHelper.addMonth(date: "2020-02-10 12:07:12+0700", format: "yyyy-MM-dd HH:mm:ssZ", localeId: "", timezoneId: "id", value: 1)
    ```
    */
    public static func addMonth(dateStr: String, format: String, localeId: String, timezoneId: String, value: Int) -> String {
        let date = stringToDate(dateStr: dateStr, format: format, localeId: localeId, timezoneId: timezoneId)
        let dateValue = Calendar.current.date(byAdding: .month, value: value, to: date) ?? Date()
        return dateToString(date: dateValue, format: format, localeId: localeId, timezoneId: timezoneId)
    }
    
    /**
       Call this function to add date based on  calendar component
       - Parameters:
           - date: original date
           - value: value to added
           - component: calendar component
     
    ### Usage Examples: ###
    ```
    let date = GITSDateHelper.addDate(date: Date(), value: 1, component: .day)
    ```
    */
    public static func addDate(date: Date, value: Int, component: Calendar.Component) -> Date {
        let dateValue = Calendar.current.date(byAdding: component, value: value, to: date) ?? Date()
        return dateValue
    }
    
    /**
       Call this function to get differences between certain time and now
       - Parameters:
           - dateStr: original date.
           - format: original date format.
           - localeId: locale with the specified identifier.
           - timezoneId: values represent geopolitical regions.
     
    ### Usage Examples: ###
    ```
    let diffDate = GITSDateHelper.timeAgoStringFrom(dateStr: "2020-02-10 14:19:34+0700", format: "dd MMM yyyy", localeId: "", timezoneId: "id")
    ```
    */
    public static func timeAgoStringFrom(dateStr: String, format: String, localeId: String, timezoneId: String) -> String {
        let dateFormat = getDateFormatter(localeId: localeId, timezoneId: timezoneId, format: format)
        let dateResult = dateFormat.date(from: dateStr)
        if dateResult != nil {
            let formatter = DateComponentsFormatter()
            formatter.unitsStyle = .full
            let now = Date()
            let calendar = Calendar.current
            let components = calendar.dateComponents(Set<Calendar.Component>([.year, .month, .weekOfMonth, .day, .hour, .minute, .second]), from: dateResult!, to: now)
            
            var isMonth = false
            var isYear = false
            var isYesterday = false
            
            if components.year! > 0 {
                isYear = true
                dateFormat.dateFormat = "MMM dd, yyyy"
                formatter.allowedUnits = .year
            } else if components.month! > 0 {
                isMonth = true
                dateFormat.dateFormat = "MMM dd"
                formatter.allowedUnits = .month
            } else if components.weekOfMonth! > 0 {
                formatter.allowedUnits = .weekOfMonth
            } else if components.day! > 0 {
                formatter.allowedUnits = .day
                if components.day! == 1 {
                    isYesterday = true
                }
            } else if components.hour! > 0 {
                formatter.allowedUnits = .hour
            } else if components.minute! > 0 {
                formatter.allowedUnits = .minute
            } else {
                formatter.allowedUnits = .second
            }
            let formatString = NSLocalizedString("%@ ago", comment: "Used to say how much time has passed. e.g. '2 hours ago'")
            
            guard let timeString = formatter.string(from: components) else {
                return "Error"
            }
            if isMonth || isYear {
                return dateFormat.string(from: dateResult!)
            } else if isYesterday {
                return "Yesterday"
            } else {
                return String(format: formatString, timeString)
            }
        } else {
            return "Error Date"
        }
    }
    
    /**
       Call this function to get local GMT from date
       - Parameters:
           - date: original date.
     
    ### Usage Examples: ###
    ```
    let localGMT = GITSDateHelper.getLocalGMT(date: Date())    
    ```
    */
    public static func getLocalGMT(date: Date) -> String {
        var zoneStr: String?
        let timeZone = DateFormatter().timeZone.secondsFromGMT(for: date)/3600
        switch timeZone {
        case 7:
            zoneStr = "WIB"
        case 8:
            zoneStr = "WITA"
        case 9:
            zoneStr = "WIT"
        default:
            zoneStr = "-"
        }
        return zoneStr ?? "-"
    }
    
    /**
       Call this function to get day parts from hour
       - Parameters:
           - hour: original hour.
     
    ### Usage Examples: ###
    ```
    let dayParts = GITSDateHelper.getDayParts(hour: "08")
    ```
    */
    public static func getDayParts(start: String, end: String) -> String {
        let startInt = Int(start) ?? 0
        let endInt = Int(end) ?? 0
        if !(startInt >= 18 || endInt <= 6) {
            return "Pagi"
        } else {
            return "Malam"
        }
    }
    
    /**
       Call this function to convert to seconds
       - Parameters:
           - time: original hour & minute.
     
    ### Usage Examples: ###
    ```
    let dayParts = GITSDateHelper.convertToSeconds(time: "17:00")
    ```
    */
    public static func convertToSeconds(time: String) -> Int {
        let timeArr = time.components(separatedBy: ":")
        let hour = Int(timeArr.first ?? "0") ?? 0
        let minute = Int(timeArr.last ?? "0") ?? 0
        return (hour * 3600) + (minute * 60)
    }
    
    /**
       Call this function to get years array data
     
    ### Usage Examples: ###
    ```
    let yearsData = GITSDateHelper.getYearsTillNow()
    ```
    */
    public static func getYearsTillNow()-> [String] {
        if let currentYear = Calendar.current.dateComponents([.year], from: Date()).year {
            var years = [String]()
            for i in (1970..<currentYear+1).reversed() {
                years.append("\(i)")
            }
            return years
        }
        return []
    }
    
    /**
     Get the amount of days between two NSDates
     
    ### Usage Examples: ###
    ```
    let rangeDate = GITSDateHelper.daysBetween(Date(), Date())
    ```
    */
    public static func daysBetween(start: Date, end: Date) -> Int {
        let calendar = Calendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: start)
        let date2 = calendar.startOfDay(for: end)
        
        let a = calendar.dateComponents([.day], from: date1, to: date2)
        return abs(a.value(for: .day)!)
    }
    
    /**
     Get the amount of days, hour, minute between two NSDates
     
    ### Usage Examples: ###
    ```
    let rangeDate = GITSDateHelper.daysBetween(Date(), Date())
    ```
    */
    public static func daysHourMinuteBetween(start: Date, end: Date) -> (Int, Int, Int) {
        let calendar = Calendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: start)
        let date2 = calendar.startOfDay(for: end)
        
        let a = calendar.dateComponents([.day, .hour, .minute], from: date1, to: date2)
        return (abs(a.value(for: .day)!), abs(a.value(for: .hour)!), abs(a.value(for: .minute)!))
    }
}
