//
//  GalleryHelper.swift
//
//
//  Created by Ajie Pramono Arganata on 12/09/19.
//  Copyright © 2019 GITS Indonesia. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

public class GalleryHelper: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    private static var delegate = GalleryHelper()
    private var onChooseImageAction: ((_ image: UIImage)->())?
    private var onChooseVideoAction: ((_ url: URL)-> ())?
    
    internal static func isAuthorizationLibraryStatus(type: UIImagePickerController.SourceType, handler: @escaping (Bool)->()) {
        if type == .camera {
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
            switch cameraAuthorizationStatus {
            case .notDetermined: requestCameraPermission(handler: handler)
            case .authorized: handler(true)
            case .restricted, .denied: showDeniedDialog(isCamera: true)
            default:
                break
            }
        } else if type == .photoLibrary {
            if PHPhotoLibrary.authorizationStatus() == .notDetermined {
                DispatchQueue.main.async {
                    PHPhotoLibrary.requestAuthorization { (status) in
                        handler(status == .authorized)
                    }
                }
            } else {
                handler(PHPhotoLibrary.authorizationStatus() == .authorized )
            }
        }
    }
    
    private static func requestCameraPermission(handler: @escaping (Bool)->()) {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted else { return }
            handler(accessGranted)
        })
    }
    
    //get image from source type
    public static func getImage(from rootVC: UIViewController, fromSourceType sourceType: UIImagePickerController.SourceType, onChooseImageAction: ((_ image: UIImage)->())?) {
        //Check is source type available
        self.isAuthorizationLibraryStatus(type: sourceType, handler: { (status) in
            if status {
                if UIImagePickerController.isSourceTypeAvailable(sourceType) {
                    DispatchQueue.main.async {
                        let imagePickerController = UIImagePickerController()
                        imagePickerController.delegate = self.delegate
                        self.delegate.onChooseImageAction = onChooseImageAction
                        imagePickerController.sourceType = sourceType
                        imagePickerController.allowsEditing = false
                        imagePickerController.modalPresentationStyle = .overFullScreen
                        rootVC.present(imagePickerController, animated: true, completion: nil)
                    }
                }
            } else {
                self.showDeniedDialog(isCamera: sourceType == .camera)
            }
        })
    }
    
    //get video from source type
    public static func getVideo(from rootVC: UIViewController, fromSourceType sourceType: UIImagePickerController.SourceType, onChooseVideoAction: ((_ url: URL)->())?) {
        //Check is source type available
        self.isAuthorizationLibraryStatus(type: sourceType, handler: { (status) in
            if status {
                if UIImagePickerController.isSourceTypeAvailable(sourceType) {
                    DispatchQueue.main.async {
                        let imagePickerController = UIImagePickerController()
                        imagePickerController.delegate = self.delegate
                        self.delegate.onChooseVideoAction = onChooseVideoAction
                        imagePickerController.sourceType = sourceType
                        imagePickerController.allowsEditing = false
                        imagePickerController.mediaTypes = ["public.movie"]
                        imagePickerController.videoQuality = .typeMedium
                        imagePickerController.modalPresentationStyle = .overFullScreen
                        rootVC.present(imagePickerController, animated: true, completion: nil)
                    }
                }
            } else {
                self.showDeniedDialog(isCamera: sourceType == .camera)
            }
        })
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            //            image.resizeImage(withPercentage: 0.7, ofImage: image, belowKb: 500) { (result) in
            picker.dismiss(animated: false)
            self.onChooseImageAction?(image)
            //            }
        } else if let url = info[.mediaURL] as? URL {
            picker.dismiss(animated: false)
            self.onChooseVideoAction?(url)
        }
        
    }
    
    private static func showDeniedDialog(isCamera: Bool) {
        let alert = UIAlertController(title: "Permission Dialog", message: "Your \(!isCamera ? "photo library" : "camera") permission is denied, you must change it on the setting menu", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Open Settings", style: .default, handler: { (action) in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl)
            }
        }))
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            UIApplication.shared.windows.last?.rootViewController?.present(alert, animated: true, completion: nil)
        })
    }
}
