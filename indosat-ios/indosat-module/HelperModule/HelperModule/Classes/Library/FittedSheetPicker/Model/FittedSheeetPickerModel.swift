//
//  FittedSheeetPickerModel.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 02/12/20.
//  Copyright © 2020 GITS Indonesia. All rights reserved.
//

import Foundation

public enum FittedSheeetPickerType {
    case countryCode
    case string
    case zone
}
public struct FittedSheeetPickerModel {
    var title: String  = ""
    var type: FittedSheeetPickerType = .string
    var data: [Any] = []
    var selectedIdx: Int?
    
    public init(title: String, type: FittedSheeetPickerType, data: [Any], selectedIdx: Int?) {
        self.title = title
        self.type = type
        self.data = data
        self.selectedIdx = selectedIdx
    }
}

public struct CountryModel: Equatable {
    public var name: String?
    public var code: String?
    public var image: String?
    
    public init(name: String? ,code: String?, image: String?) {
        self.name = name
        self.code = code
        self.image = image
    }
}
