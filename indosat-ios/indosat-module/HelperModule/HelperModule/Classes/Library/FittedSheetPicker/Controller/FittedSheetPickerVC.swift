//
//  FittedSheetPickerVC.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 02/12/20.
//  Copyright © 2020 GITS Indonesia. All rights reserved.
//

import UIKit
import FittedSheets
import IGListKit

public class FittedSheetPickerVC: BaseVC {
    // MARK: Properties
    /// FittedSheetPicker View
    private var fittedSheetPickerView: FittedSheetPickerView!
    /// FittedSheetPicker Data Source
    private lazy var dataSource = FittedSheetPickerDataSource()
    /// FittedSheetPicker Adapter
    private lazy var adapter: ListAdapter = {
        return ListAdapter(
            updater: ListAdapterUpdater(),
            viewController: self,
            workingRangeSize: 0)
    }()
    private var item: FittedSheeetPickerModel?
    private var completion: ((_ picker: UIViewController, _ section: Int, _ idx: Int)->())?
    
    // MARK: Function
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
        self.view.backgroundColor = .white
    }
    
    /** Configure Something here
     */
    public override func configureView() {
        self.fittedSheetPickerView = FittedSheetPickerView(frame: .zero)
        self.fittedSheetPickerView.instantiateView(from: self.view)
        self.fittedSheetPickerView.item = self.item
        self.fittedSheetPickerView.setButtonHandler { [weak self](view) in
            guard let strongSelf = self else { return }
            if view == strongSelf.fittedSheetPickerView.closeBtn {
                strongSelf.dismiss(animated: true, completion: nil)
            }
        }
        self.adapter.collectionView = self.fittedSheetPickerView.collectionVw
        self.adapter.dataSource = self.dataSource
        
        self.dataSource.sectionSelectedItem { [weak self](section, code) in
            guard let strongSelf = self else { return }
            strongSelf.completion?(strongSelf, section, code)
        }
    }
    
    private func setData(item: FittedSheeetPickerModel) {
        self.item = item
        self.dataSource.addSectionListData(item: item)
    }
    
    public static func openFittedSheet(caller: UIViewController, item: FittedSheeetPickerModel, completion: ((_ picker: UIViewController, _ section: Int, _ idx: Int)->())?) {
        let vc = FittedSheetPickerVC()
        let sheetController = SheetViewController(controller: vc)
        sheetController.topCornersRadius = 30
        sheetController.extendBackgroundBehindHandle = true
        vc.setData(item: item)
        vc.completion = completion
        caller.present(sheetController, animated: true, completion: nil)
    }
}

