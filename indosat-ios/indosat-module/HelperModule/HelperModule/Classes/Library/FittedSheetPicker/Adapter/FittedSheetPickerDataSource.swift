//
//  FittedSheetPickerDataSource.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 02/12/20.
//  Copyright © 2020 GITS Indonesia. All rights reserved.
//

import UIKit
import IGListKit

/// FittedSheetPicker Data Source is a class for collection data source
class FittedSheetPickerDataSource: NSObject, ListAdapterDataSource {
    /// The Section List
    private var sectionList = FittedSheetPickerListSectionController()
    
    func sectionSelectedItem(onSelectedItem: ((_ section: Int, _ idx: Int)->())?) {
        self.sectionList.onSelectedItem = onSelectedItem
    }
    
    func addSectionListData(item: FittedSheeetPickerModel) {
        self.sectionList.item = item
        self.refreshSection()
    }
    
    func refreshSection() {
        self.sectionList.onRefreshDataSection?()
    }
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return ["list"] as [ListDiffable]
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return self.sectionList
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}
