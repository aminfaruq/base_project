//
//  FittedSheetPickerListSectionController.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 02/12/20.
//  Copyright © 2020 GITS Indonesia. All rights reserved.
//

import UIKit
import IGListKit

class FittedSheetPickerListSectionController: BaseListSectionController {
    /// Items
    public var item: FittedSheeetPickerModel?
    
    // MARK: Function
    /** Initialization
     */
    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        self.minimumInteritemSpacing = 0
        self.minimumLineSpacing = 0
        self.onRefreshDataSection = {
            self.collectionContext?.performBatch(animated: true, updates: { batchContext in
              batchContext.reload(self)
            }, completion: nil)
        }
    }
    
    /** Number of items in one section
     */
    override func numberOfItems() -> Int {
        return self.item?.data.count ?? 0
    }
    
    /** Set the cell here
    */
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        if item?.type == .countryCode {
            let cell = FittedSheetPickerCountryCollectionCell.initCell(self.collectionContext, section: self, index: index, item: self.item?.data[index]) as! FittedSheetPickerCountryCollectionCell
            cell.setButtonHandler { [weak self](view) in
                guard let strongSelf = self else { return }
                strongSelf.item?.selectedIdx = index
                strongSelf.onSelectedItem?(strongSelf.section, index)
            }
            return cell
        } else if item?.type == .zone {
            let cell = FittedSheetPickerZoneCollectionCell.initCell(self.collectionContext, section: self, index: index, item: self.item?.data[index]) as! FittedSheetPickerZoneCollectionCell
            cell.isSelected = self.item?.selectedIdx == index
            cell.setButtonHandler { [weak self](view) in
                guard let strongSelf = self else { return }
                strongSelf.item?.selectedIdx = index
                strongSelf.onSelectedItem?(strongSelf.section, index)
                strongSelf.onRefreshDataSection?()
            }
            return cell
        } else {
            let cell = FittedSheetPickerStringCollectionCell.initCell(self.collectionContext, section: self, index: index, item: self.item?.data[index]) as! FittedSheetPickerStringCollectionCell
            cell.isSelected = self.item?.selectedIdx == index
            cell.selectedCB()
            cell.setButtonHandler { [weak self](view) in
                guard let strongSelf = self else { return }
                strongSelf.item?.selectedIdx = index
                strongSelf.onSelectedItem?(strongSelf.section, index)
                strongSelf.onRefreshDataSection?()
            }
            return cell
        }
        
    }
    
    /** Size of the item
    */
    override func sizeForItem(at index: Int) -> CGSize {
        if item?.type == .countryCode {
            return FittedSheetPickerCountryCollectionCell.cellSize(screenWidth: self.collectionContext?.containerSize.width ?? 0, item: self.item?.data[index])
        } else if item?.type == .zone {
            return FittedSheetPickerZoneCollectionCell.cellSize(screenWidth: self.collectionContext?.containerSize.width ?? 0, item: self.item?.data[index])
        } else {
            return FittedSheetPickerStringCollectionCell.cellSize(screenWidth: self.collectionContext?.containerSize.width ?? 0, item: self.item?.data[index])
        }
    }
}
