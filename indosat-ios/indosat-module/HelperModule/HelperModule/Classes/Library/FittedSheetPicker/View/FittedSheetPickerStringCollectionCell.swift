//
//  FittedSheetPickerStringCollectionCell.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 02/12/20.
//  Copyright © 2020 GITS Indonesia. All rights reserved.
//

import UIKit
import M13Checkbox

class FittedSheetPickerStringCollectionCell: BaseCollectionCell {
    // MARK: Properties
    private var titleLbl: UILabel!
    private var cb: M13Checkbox!
    private var lineVw: UIView!
    private var btn: CustomButton!
    /// Item
    public override var item: Any? {
        didSet {
            guard let item = item as? String else { return }
            self.titleLbl.text = item
            self.cb.checkState = self.isSelected ? .checked : .unchecked
        }
    }
    
    // MARK: Function
    
    /** Initialization Frame, will setup the component to view
     */
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configContent()
        self.updateConstraints()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /** Initialization the constraint
    */
    override func initConstraint() {
        self.titleLbl.autoPinEdge(toSuperviewEdge: .top, withInset: 24)
        self.titleLbl.autoPinEdge(toSuperviewEdge: .left)
        
        self.cb.autoAlignAxis(.horizontal, toSameAxisOf: self.titleLbl)
        self.cb.autoPinEdge(toSuperviewEdge: .right)
        self.cb.autoPinEdge(.left, to: .right, of: self.titleLbl, withOffset: 16, relation: .greaterThanOrEqual)
        self.cb.autoSetDimensions(to: CGSize(width: 20, height: 20))
        
        self.lineVw.autoSetDimension(.height, toSize: 1)
        self.lineVw.autoPinEdge(.top, to: .bottom, of: self.titleLbl, withOffset: 8)
        self.lineVw.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
        
        self.btn.autoPinEdgesToSuperviewEdges()
    }
    
    /** Set Button Handler
     */
    override func setButtonHandler(handler: ((UIView) -> ())?) {
        self.btn.addHandlerButton(handler: handler)
    }
    
    /** Content Cell Size
    */
    override class func cellSize(screenWidth: CGFloat, item: Any?) -> CGSize {
        guard let item = item as? String else { return .zero }
        let leftRightPaddingSection: CGFloat = 32 // 16 Left, 16 Right
        let width = screenWidth - leftRightPaddingSection
        let widthCb: CGFloat = 20
        let heightLabel = TextSize.size(item, font: UIFont.Poppins.regular.font(ofSize: 16), width: width-(16-widthCb), numberOfLines: 0).height
        let marginTopLabel: CGFloat = 24
        let heightLine: CGFloat = 1
        let marginTopLine: CGFloat = 8
        let size = CGSize(width: width, height: heightLabel + heightLine + marginTopLine + marginTopLabel)
        return size
    }
}

extension FittedSheetPickerStringCollectionCell {
    /** Content View
    */
    private func configContent() {
        self.titleLbl = UILabel.customLabel(text: "Title", line: 1, font: UIFont.Poppins.regular.font(ofSize: 16), color: .black, alignment: .left)
        self.contentView.addSubview(self.titleLbl)
        
        self.cb = M13Checkbox(frame: .zero)
        self.cb.boxType = .circle
        self.cb.markType = .radio
        self.cb.stateChangeAnimation = .fill
        self.cb.tintColor = UIColor("#009688")
        self.cb.isUserInteractionEnabled = false
        self.contentView.addSubview(self.cb)
        
        self.lineVw = UIView(frame: .zero)
        self.lineVw.backgroundColor = UIColor("#EDF1F4")
        self.contentView.addSubview(self.lineVw)
        
        self.btn = CustomButton(frame: .zero)
        self.btn.applyStyleFont(title: "", font: nil, fontColor: .white)
        self.btn.applyStyleBackground(backgroundColor: .clear)
        self.btn.applyStyleBorder(shadow: 0, borderRadius: 6)
        self.contentView.addSubview(self.btn)
    }
    
    public func selectedCB() {
        self.cb.checkState = self.isSelected ? .checked : .unchecked
    }
}
