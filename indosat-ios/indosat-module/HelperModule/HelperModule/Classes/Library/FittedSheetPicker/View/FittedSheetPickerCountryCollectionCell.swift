//
//  FittedSheetPickerCountryCollectionCell.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 02/12/20.
//  Copyright © 2020 GITS Indonesia. All rights reserved.
//

import UIKit
import M13Checkbox

class FittedSheetPickerCountryCollectionCell: BaseCollectionCell {
    // MARK: Properties
    private var imageVw: UIImageView!
    private var titleLbl: UILabel!
    private var codeLbl: UILabel!
    private var lineVw: UIView!
    private var btn: CustomButton!
    /// Item
    public override var item: Any? {
        didSet {
            guard let item = item as? CountryModel else { return }
            self.titleLbl.text = item.name ?? ""
            self.codeLbl.text = item.code ?? ""
            self.imageVw.setImageWith(url: item.image)
        }
    }
    
    // MARK: Function
    
    /** Initialization Frame, will setup the component to view
     */
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configContent()
        self.updateConstraints()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /** Initialization the constraint
    */
    override func initConstraint() {
        self.titleLbl.autoPinEdge(toSuperviewEdge: .top, withInset: 24)
        
        self.imageVw.autoPinEdge(toSuperviewEdge: .left)
        self.imageVw.autoSetDimensions(to: CGSize(width: 20, height: 20))
        self.imageVw.autoAlignAxis(.horizontal, toSameAxisOf: self.titleLbl)
        self.titleLbl.autoPinEdge(.left, to: .right, of: self.imageVw, withOffset: 16)
        
        self.codeLbl.autoAlignAxis(.horizontal, toSameAxisOf: self.titleLbl)
        self.codeLbl.autoPinEdge(toSuperviewEdge: .right)
        self.codeLbl.autoPinEdge(.left, to: .right, of: self.titleLbl, withOffset: 16, relation: .greaterThanOrEqual)
       
        self.lineVw.autoSetDimension(.height, toSize: 1)
        self.lineVw.autoPinEdge(.top, to: .bottom, of: self.titleLbl, withOffset: 8)
        self.lineVw.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
        
        self.btn.autoPinEdgesToSuperviewEdges()
    }
    
    /** Set Button Handler
     */
    override func setButtonHandler(handler: ((UIView) -> ())?) {
        self.btn.addHandlerButton(handler: handler)
    }
    
    /** Content Cell Size
    */
    override class func cellSize(screenWidth: CGFloat, item: Any?) -> CGSize {
        guard let item = item as? CountryModel else { return .zero }
        let leftRightPaddingSection: CGFloat = 32 // 16 Left, 16 Right
        let width = screenWidth - leftRightPaddingSection
        let widthImage: CGFloat = 20
        let heightLabel = TextSize.size((item.name ?? "") + (item.code ?? ""), font: UIFont.Poppins.regular.font(ofSize: 16), width: width-(32-widthImage), numberOfLines: 0).height
        let marginTopLabel: CGFloat = 24
        let heightLine: CGFloat = 1
        let marginTopLine: CGFloat = 8
        let size = CGSize(width: width, height: heightLabel + heightLine + marginTopLine + marginTopLabel)
        return size
    }
}

extension FittedSheetPickerCountryCollectionCell {
    /** Content View
    */
    private func configContent() {
        self.imageVw = UIImageView(frame: .zero)
        self.imageVw.contentMode = .scaleAspectFit
        self.imageVw.clipsToBounds = true
        self.contentView.addSubview(self.imageVw)
        
        self.titleLbl = UILabel.customLabel(text: "Title", line: 1, font: UIFont.Poppins.regular.font(ofSize: 16), color: .black, alignment: .left)
        self.contentView.addSubview(self.titleLbl)
        
        self.codeLbl = UILabel.customLabel(text: "Code", line: 1, font: UIFont.Poppins.regular.font(ofSize: 16), color: .black, alignment: .right)
        self.contentView.addSubview(self.codeLbl)
        
        self.lineVw = UIView(frame: .zero)
        self.lineVw.backgroundColor = UIColor("#EDF1F4")
        self.contentView.addSubview(self.lineVw)
        
        self.btn = CustomButton(frame: .zero)
        self.btn.applyStyleFont(title: "", font: nil, fontColor: .white)
        self.btn.applyStyleBackground(backgroundColor: .clear)
        self.btn.applyStyleBorder(shadow: 0, borderRadius: 6)
        self.contentView.addSubview(self.btn)
    }
}
