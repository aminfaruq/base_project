//
//  FittedSheetPickerZoneCollectionCell.swift
//  HelperModule
//
//  Created by Ajie Arganata on 22/02/21.
//

import UIKit
import M13Checkbox

class FittedSheetPickerZoneCollectionCell: BaseCollectionCell {
    // MARK: Properties
    private var titleLbl: UILabel!
    private var priceLbl: UILabel!
    private var lineVw: UIView!
    private var btn: CustomButton!
    /// Item
    public override var item: Any? {
        didSet {
            guard let item = item as? [String: Any?] else { return }
            self.titleLbl.text = item["name"] as? String
            self.priceLbl.text = item["price"] as? String
        }
    }
    
    // MARK: Function
    
    /** Initialization Frame, will setup the component to view
     */
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configContent()
        self.updateConstraints()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /** Initialization the constraint
    */
    override func initConstraint() {
        self.titleLbl.autoPinEdge(toSuperviewEdge: .top, withInset: 24)
        self.titleLbl.autoPinEdge(toSuperviewEdge: .left)
        self.titleLbl.autoPinEdge(toSuperviewEdge: .right)
        
        self.priceLbl.autoPinEdge(.top, to: .bottom, of: self.titleLbl, withOffset: 2)
        self.priceLbl.autoPinEdge(toSuperviewEdge: .left)
        self.priceLbl.autoPinEdge(toSuperviewEdge: .right)
        
        self.lineVw.autoSetDimension(.height, toSize: 1)
        self.lineVw.autoPinEdge(.top, to: .bottom, of: self.priceLbl, withOffset: 8)
        self.lineVw.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
        
        self.btn.autoPinEdgesToSuperviewEdges()
    }
    
    /** Set Button Handler
     */
    override func setButtonHandler(handler: ((UIView) -> ())?) {
        self.btn.addHandlerButton(handler: handler)
    }
    
    /** Content Cell Size
    */
    override class func cellSize(screenWidth: CGFloat, item: Any?) -> CGSize {
        guard let item = item as? [String: Any?] else { return .zero }
        let titleLbl = item["name"] as? String
        let priceLbl = item["price"] as? String
        
        let leftRightPaddingSection: CGFloat = 32 // 16 Left, 16 Right
        let width = screenWidth - leftRightPaddingSection
        let heightLabel = TextSize.size(titleLbl ?? "", font: UIFont.Poppins.regular.font(ofSize: 16), width: width, numberOfLines: 1).height
        let heightPriceLabel = TextSize.size(priceLbl ?? "", font: UIFont.Poppins.regular.font(ofSize: 16), width: width, numberOfLines: 1).height
        let marginTopLabel: CGFloat = 24
        let heightLine: CGFloat = 1
        let marginTopLine: CGFloat = 8
        let size = CGSize(width: width, height: heightLabel + 2 + heightPriceLabel + heightLine + marginTopLine + marginTopLabel)
        return size
    }
}

extension FittedSheetPickerZoneCollectionCell {
    /** Content View
    */
    private func configContent() {
        self.titleLbl = UILabel.customLabel(text: "Title", line: 1, font: UIFont.Poppins.regular.font(ofSize: 16), color: .black, alignment: .left)
        self.contentView.addSubview(self.titleLbl)
        
        self.priceLbl = UILabel.customLabel(text: "Rp 0", line: 1, font: UIFont.Poppins.regular.font(ofSize: 14), color: .colorPrimary, alignment: .left)
        self.contentView.addSubview(self.priceLbl)
        
        self.lineVw = UIView(frame: .zero)
        self.lineVw.backgroundColor = UIColor("#EDF1F4")
        self.contentView.addSubview(self.lineVw)
        
        self.btn = CustomButton(frame: .zero)
        self.btn.applyStyleFont(title: "", font: nil, fontColor: .white)
        self.btn.applyStyleBackground(backgroundColor: .clear)
        self.btn.applyStyleBorder(shadow: 0, borderRadius: 6)
        self.contentView.addSubview(self.btn)
    }
}
