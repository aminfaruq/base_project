//
//  FittedSheetPickerView.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 02/12/20.
//  Copyright © 2020 GITS Indonesia. All rights reserved.
//

import UIKit
import IGListKit

class FittedSheetPickerView: BaseCustomView {
    // MARK: Properties
    /// Title
    private var title: UILabel!
    /// Close Btn
    public var closeBtn: CustomButton!
    /// A Collection View
    public var collectionVw: UICollectionView!
    /// Item
    override var item: Any? {
        didSet {
            guard let item = self.item as? FittedSheeetPickerModel else { return }
            self.title.text = item.title
        }
    }
    
    // MARK: Function
    /** Initialization Frame, will setup the component to view
     */
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configCollection()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func setButtonHandler(handler: ((UIView) -> ())?) {
        self.closeBtn.addHandlerButton(handler: handler)
    }
    
    /** Initialization the constraint
     */
    override func initConstraint() {
        self.title.autoPinEdge(toSuperviewEdge: .top, withInset: 16)
        self.title.autoPinEdge(toSuperviewEdge: .left, withInset: 16)
        self.closeBtn.autoPinEdge(.left, to: .right, of: self.title, withOffset: 16)
        self.closeBtn.autoSetDimensions(to: CGSize(width: 24, height: 24))
        self.closeBtn.autoAlignAxis(.horizontal, toSameAxisOf: self.title)
        self.closeBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 16)
        self.collectionVw.autoPinEdge(.top, to: .bottom, of: self.title, withOffset: 0)
        self.collectionVw.autoPinEdgesToSuperviewSafeArea(with: .zero, excludingEdge: .top)
    }
}

extension FittedSheetPickerView {
    /** Configure View Collection
     */
    private func configCollection() {
        self.title = UILabel.customLabel(text: "Title", line: 0, font: UIFont.Poppins.medium.font(ofSize: 24), color: .black, alignment: .left)
        self.addSubview(self.title)
        
        self.closeBtn = CustomButton(frame: .zero)
        self.closeBtn.applyStyleBorder(shadow: 0, borderRadius: 6)
        self.closeBtn.setImage(UIImage.loadLocalImage(bundle: HelperBundle.bundle, moduleName: HelperBundle.moduleName, name: "ic_close", ext: "png"), for: .normal)
        self.addSubview(self.closeBtn)
        
        let collectionFlowLayout = ListCollectionViewLayout.init(stickyHeaders: false, scrollDirection: .vertical, topContentInset: 0, stretchToEdge: true)
        self.collectionVw = UICollectionView(frame: .zero, collectionViewLayout: collectionFlowLayout)
        self.collectionVw.alwaysBounceVertical = true
        self.collectionVw.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0)
        
        self.collectionVw.backgroundColor = .white
        self.backgroundColor = .white

        self.addSubview(collectionVw)
    }
}
