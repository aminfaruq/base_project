//
//  EmptyView.swift
//  HelperModule
//
//  Created by Ajie Arganata on 05/04/21.
//

import UIKit


public class EmptyView: BaseCustomView {
    // MARK: Properties
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView(frame: .zero)
        scrollView.showsVerticalScrollIndicator = false
        scrollView.alwaysBounceVertical = true
        return scrollView
    }()
    private lazy var contentView: UIView = {
        let contentView = UIView(frame: .zero)
        return contentView
    }()
    private lazy var imageVw: UIImageView = {
        let imageVw = UIImageView(frame: .zero)
        imageVw.clipsToBounds = true
        imageVw.contentMode = .scaleAspectFit
        return imageVw
    }()
    private lazy var titleLbl: UILabel = {
        return UILabel.customLabel(text: "Title", line: 0, font: UIFont.Poppins.medium.font(ofSize: 16), color: .colorBlackPrimary, alignment: .center)
    }()
    private lazy var descLbl: UILabel = {
        return UILabel.customLabel(text: "Description", line: 0, font: UIFont.Poppins.regular.font(ofSize: 14), color: .colorLightGray, alignment: .center)
    }()
    
    // MARK: Function
    
    /** Init Frame
     */
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configSubview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /** Init Constraint
     */
    public override func initConstraint() {
        // Container View
        self.scrollView.autoPinEdgesToSuperviewSafeArea()
        self.contentView.autoPinEdgesToSuperviewEdges()
        self.contentView.autoMatch(.width, to: .width, of: self.scrollView)
        NSLayoutConstraint.autoSetPriority(.defaultLow) { [weak self] in
            guard let self = self else { return }
            self.contentView.autoMatch(.height, to: .height, of: self.scrollView)
        }
        // End Container View
        self.imageVw.autoSetDimension(.height, toSize: 126)
        self.imageVw.autoPinEdge(toSuperviewEdge: .left, withInset: 24)
        self.imageVw.autoPinEdge(toSuperviewEdge: .right, withInset: 24)
        self.imageVw.autoPinEdge(toSuperviewEdge: .top, withInset: 24, relation: .greaterThanOrEqual)
        NSLayoutConstraint.autoSetPriority(.defaultLow) {
            self.imageVw.autoAlignAxis(.horizontal, toSameAxisOf: self.contentView, withOffset: -84)
        }
        
        self.titleLbl.autoPinEdge(.top, to: .bottom, of: self.imageVw, withOffset: 16)
        self.titleLbl.autoPinEdge(toSuperviewEdge: .left, withInset: 24)
        self.titleLbl.autoPinEdge(toSuperviewEdge: .right, withInset: 24)
        
        self.descLbl.autoPinEdge(.top, to: .bottom, of: self.titleLbl, withOffset: 8)
        self.descLbl.autoPinEdge(toSuperviewEdge: .left, withInset: 24)
        self.descLbl.autoPinEdge(toSuperviewEdge: .right, withInset: 24)
        self.descLbl.autoPinEdge(toSuperviewEdge: .bottom, withInset: 24, relation: .greaterThanOrEqual)
    }
}

extension EmptyView {
    /** Config Subview
     */
    private func configSubview() {
        self.addSubview(self.scrollView)
        self.scrollView.addSubview(self.contentView)
        self.contentView.addSubview(self.imageVw)
        self.contentView.addSubview(self.titleLbl)
        self.contentView.addSubview(self.descLbl)
    }
    
    public func setup(title: String, desc: String, image: String) {
        self.titleLbl.text = title
        self.descLbl.text = desc
        self.imageVw.image = UIImage.loadLocalImage(bundle: HelperBundle.bundle, moduleName: HelperBundle.moduleName, name: image, ext: "png")
    }
}
