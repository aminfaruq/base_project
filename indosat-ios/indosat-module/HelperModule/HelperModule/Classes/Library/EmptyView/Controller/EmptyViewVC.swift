//
//  EmptyViewVC.swift
//  HelperModule
//
//  Created by Ajie Arganata on 05/04/21.
//

import UIKit

public class EmptyViewVC: BaseVC {
    // MARK: Properties
    private var contentView = EmptyView()
    
    // MARK: Function
    
    /** ViewDidLoad
     */
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    /** Config The View
     */
    public override func configureView() {
        self.contentView.instantiateView(from: self.view)
    }
}
