//
//  KeychainService.swift
//  BiometricLoginApp
//
//  Created by IRFAN TRIHANDOKO on 20/11/20.
//

import Foundation
import Security

// Type of Actions
public enum KeychainServiceEnum {
    case save
    case update
    case load
    case remove
}

public class KeychainService {
    
    // Constant Identifiers
    private static let userAccount = "MovicAuthenticatedUser"
    private static let accessGroup = "MovicSecurityService"
    /**
     *  User defined keys for new entry
     *  Note: add new keys for new secure item and use them in load and save methods
     */
    
    //  private static let userDataKey = "KeyUserDataMovic"
    
    // Arguments for the keychain queries
    private static let kSecClassValue = NSString(format: kSecClass)
    private static let kSecAttrAccountValue = NSString(format: kSecAttrAccount)
    private static let kSecValueDataValue = NSString(format: kSecValueData)
    private static let kSecClassGenericPasswordValue = NSString(format: kSecClassGenericPassword)
    private static let kSecAttrServiceValue = NSString(format: kSecAttrService)
    private static let kSecMatchLimitValue = NSString(format: kSecMatchLimit)
    private static let kSecReturnDataValue = NSString(format: kSecReturnData)
    private static let kSecMatchLimitOneValue = NSString(format: kSecMatchLimitOne)
    
    static let pref = UserDefaults.standard
    
    /**
     * Exposed methods to perform queries.
     */
    
    public static func configUserData() {
        if !self.pref.bool(forKey: "installed") {
            let response = self.remove(service: "smartLogin")
            debugPrint("Removed Data: \(response)")
            self.pref.setValue(true, forKey: "installed")
            self.pref.synchronize()
        }
    }
    
    public static func saveUserData(key: NSString, data: NSString) -> (data: NSString?, error: NSString?) {
        let response = self.save(service: key, data: data)
        return response
    }
    
    public static func updateUserData(key: NSString, data: NSString) -> (data: NSString?, error: NSString?) {
        let response = self.update(service: key, data: data)
        return response
    }
    
    public static func loadUserData(key: NSString) -> (data: NSString?, error: NSString?) {
        let response = self.load(service: key)
        return response
    }
    
    public static func removeUserData(key: NSString) -> (data: NSString?, error: NSString?) {
        let response = self.remove(service: key)
        return response
    }
    
    public static func createItemData(type: KeychainServiceEnum, service: NSString, data: NSString) -> (data: NSString?, error: NSString?) {
        let dataFromString: NSData = data.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)! as NSData
        
        // Instantiate a new default keychain query
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount, dataFromString], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecValueDataValue])
        let status  = type == .save ? SecItemAdd(keychainQuery as CFDictionary, nil) : SecItemUpdate(keychainQuery as CFDictionary, [kSecValueDataValue: dataFromString] as CFDictionary)
        
        if status == errSecSuccess {
            let message = type == .save ? "Data successfully saved." : "Data successfully updated." as NSString
            return (message, nil)
        } else {
            let message = type == .save ? "Failed to save data." : "Failed to update data." as NSString
            return (nil, message)
        }
    }
    
    /**
     * Internal methods for querying the keychain.
     */
    private static func save(service: NSString, data: NSString) -> (data: NSString?, error: NSString?) {
        let keychain = self.load(service: service)
        if keychain.data != nil {
            return self.createItemData(type: .update, service: service, data: data)
        } else {
            // Add the new keychain item
            return self.createItemData(type: .save, service: service, data: data)
        }
    }
    
    /**
     * Internal methods for updating data from the keychain.
     */
    private static func update(service: NSString, data: NSString) -> (data: NSString?, error: NSString?) {
        return self.createItemData(type: .update, service: service, data: data)
    }
    
    /**
     * Internal methods for getting data from the keychain.
     */
    private static func load(service: NSString) -> (data: NSString?, error: NSString?) {
        // Instantiate a new default keychain query
        // Tell the query to return a result
        // Limit our results to one item
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount, kCFBooleanTrue ?? true, kSecMatchLimitOneValue], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecReturnDataValue, kSecMatchLimitValue])
        
        var dataTypeRef :AnyObject?
        
        // Search for the keychain items
        let status: OSStatus = SecItemCopyMatching(keychainQuery, &dataTypeRef)
        var contentsOfKeychain: NSString? = nil
        var error: NSString? = nil
        
        if status == errSecSuccess {
            if let retrievedData = dataTypeRef as? NSData {
                contentsOfKeychain = String(data: retrievedData as Data, encoding: .utf8) as NSString?
            }
        } else {
            error = "Nothing was retrieved from the keychain. Status code \(status)." as NSString
        }
        return (contentsOfKeychain, error)
    }
    
    /**
     * Internal methods for deleting data from the keychain.
     */
    private static func remove(service: NSString) -> (data: NSString?, error: NSString?) {
        var message: NSString? = nil
        var error: NSString? = nil
        let keychain = self.load(service: service)
        if keychain.data != nil {
            let dataFromString: NSData = (keychain.data?.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false) ?? Data()) as NSData
            let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount, dataFromString], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecValueDataValue])
            
            // Delete any existing items
            let status = SecItemDelete(keychainQuery as CFDictionary)
            if (status == errSecSuccess) {
                message = "Data successfully deleted."
            } else {
                if #available(iOS 11.3, *) {
                    if let err = SecCopyErrorMessageString(status, nil) {
                        error = err as NSString
                    } else {
                        error = "Failed to remove data."
                    }
                } else {
                    // Fallback on earlier versions
                    error = "Failed to remove data."
                }
            }
        } else {
            error = "No data from keychain." as NSString
        }
        return (message, error)
    }
    
//    public static func convertDictionaryToJSONString(data: [String: Any]) -> String{
//        if let data = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted),
//           let str = String(data: data, encoding: .utf8) {
//            return str
//        }
//        return ""
//    }
//    
//    public static func convertJSONStringToDictionary(text: String) -> [String: Any] {
//        if let data = text.data(using: .utf8) {
//            do {
//                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] ?? [:]
//            } catch {
//                print(error.localizedDescription)
//            }
//        }
//        return [:]
//    }
}

