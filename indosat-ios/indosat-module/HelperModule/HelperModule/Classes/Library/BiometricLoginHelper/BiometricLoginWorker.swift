//
//  BiometricLoginWorker.swift
//  BiometricLoginApp
//
//  Created by IRFAN TRIHANDOKO on 07/12/20.
//

import Foundation
import LocalAuthentication


public class BiometricLoginWorker {
    
    public static func canEvaluatePolicy(callback: @escaping (_ response: BiometricBaseModel?) -> Void) {
        let context = LAContext()
        var data: BiometricBaseModel?
        var code: Int?
        var message: String?
        var authorizationError: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authorizationError) {
            code = 1
            message = BiometricStringConstant.successful
        } else {
            if let error = authorizationError {
                message = error.localizedDescription
                code = message?.contains("locked") ?? true ? 7 : 6
            } else {
                code = 5
                message = BiometricStringConstant.notAvailable
            }
        }
        
        data = BiometricBaseModel(code: code, message: message)
        callback(data)
    }
    
    public static func biometricType(callback: @escaping (_ response: BiometricTypeModel?) -> Void) {
        let context = LAContext()
        var data: BiometricTypeModel?
        data = BiometricTypeModel(base: BiometricBaseModel(code: 5, message: BiometricStringConstant.notAvailable), biometricType: BiometricStringConstant.none)
        
        self.canEvaluatePolicy { (response) in
            if response?.code == 1 {
                if #available(iOS 11.0, *) {
                    let enumType = context.biometryType
                    if enumType == .faceID || enumType == .touchID {
                        let type = enumType == .faceID ? BiometricStringConstant.faceID : BiometricStringConstant.touchID
                        data = BiometricTypeModel(base: BiometricBaseModel(code: 1, message: BiometricStringConstant.successful), biometricType: type)
                    }
                }
            } else {
                data = BiometricTypeModel(base: BiometricBaseModel(code: response?.code ?? 0, message: response?.message ?? ""), biometricType: BiometricStringConstant.none)
            }
        }
        
        callback(data)
    }
    
    @available(iOS 11.0, *)
    public static func evaluateError(err: Error?, _ callback: @escaping (_ response: BiometricBaseModel?) -> Void) {
        var code: Int?
        var error: String?
        
        switch err {
        case LAError.authenticationFailed?:
            code = 2
            error = BiometricStringConstant.authenticationFailed
        case LAError.userCancel?:
            code = 3
            error = BiometricStringConstant.userCancel
        case LAError.userFallback?:
            code = 4
            error = BiometricStringConstant.userFallback
        case LAError.biometryNotAvailable?:
            code = 5
            error = BiometricStringConstant.notAvailable
        case LAError.biometryNotEnrolled?:
            code = 6
            error = BiometricStringConstant.notEnrolled
        case LAError.biometryLockout?:
            code = 7
            error = BiometricStringConstant.lockout
        default:
            code = 8
            error = BiometricStringConstant.defaultError
        }
        
        let data = BiometricBaseModel(code: code ?? 0, message: error ?? "")
        callback(data)
    }
    
    public static func authenticateUser(callback: @escaping (_ response: BiometricBaseModel?) -> Void) {
        let context = LAContext()
        var data: BiometricBaseModel?
        
        self.canEvaluatePolicy { (response) in
            if response?.code == 1 {
                context.localizedFallbackTitle = ""
                context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Authentication required to access the secure data.") { (success, evaluateError) in
                    if success {
                        data = BiometricBaseModel(code: 1, message: BiometricStringConstant.successful)
                        callback(data)
                    } else {
                        var error: String?
                        if #available(iOS 11.0, *) {
                            self.evaluateError(err: evaluateError) { (response) in
                                data = BiometricBaseModel(code: response?.code ?? 0, message: response?.message)
                                callback(data)
                            }
                        } else {
                            // Fallback on earlier versions
                            error = BiometricStringConstant.notAvailable
                            data = BiometricBaseModel(code: 5, message: error ?? "")
                            callback(data)
                        }
                    }
                }
            } else {
                data = BiometricBaseModel(code: response?.code ?? 0, message: response?.message ?? "")
                callback(data)
            }
        }
    }
    
    public static func keychainCallback(type: KeychainServiceEnum, key: NSString? = nil, data: NSString? = nil, _ callback: @escaping (_ response: BiometricUserModel?) -> Void) {
        var userDataModel: BiometricUserModel?
        
        var keychain: (data: NSString?, error: NSString?)
        switch type {
        case .save:
            keychain = KeychainService.saveUserData(key: key ?? "", data: data ?? "")
        case .update:
            keychain = KeychainService.updateUserData(key: key ?? "", data: data ?? "")
        case .remove:
            keychain = KeychainService.removeUserData(key: key ?? "")
        default:
            keychain = KeychainService.loadUserData(key: key ?? "")
        }
        if keychain.data != nil {
            let code = keychain.data?.lowercased.contains("failed") ?? true ? 0 : 1
            if type == .load {
                let dict = GITSStringHelper.convertJSONStringToDictionary(text: keychain.data as String? ?? "")
                userDataModel = BiometricUserModel(base: BiometricBaseModel(code: code, message: KeychainConstant.loadSuccessful), data: dict)
            } else {
                let message = keychain.data as String?
                userDataModel = BiometricUserModel(base: BiometricBaseModel(code: code, message: message ?? "" ))
                
            }
        } else {
            let message = keychain.error as String?
            userDataModel = BiometricUserModel(base: BiometricBaseModel(code: 0, message: message ?? "" ))
        }
        
        callback(userDataModel)
    }
    
}
