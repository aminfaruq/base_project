//
//  BiometricResponseModel.swift
//  BiometricLoginApp
//
//  Created by IRFAN TRIHANDOKO on 07/12/20.
//

import Foundation

public struct BiometricStringConstant {
    public static let notAvailable = "Face ID/Touch ID is not available."
    public static let authenticationFailed = "There was a problem verifying your identity."
    public static let userCancel = "You pressed cancel."
    public static let userFallback = "You pressed password."
    public static let notEnrolled = "Face ID/Touch ID is not set up."
    public static let lockout = "Face ID/Touch ID is locked."
    public static let defaultError = "Face ID/Touch ID may not be configured"
    public static let touchID = "Touch ID"
    public static let faceID = "Face ID"
    public static let none = "None"
    public static let successful = "Successful"
}


public struct KeychainConstant {
    public static let userAccount = "MovicAuthenticatedUser"
    public static let accessGroup = "MovicSecurityService"
    public static let saveSuccessful = "Data successfully saved."
    public static let updateSuccessful = "Data successfully updated."
    public static let deleteSuccessful = "Data successfully deleted."
    public static let loadSuccessful = "Data successfully loaded."
    public static let saveFailed = "Failed to save data."
    public static let updateFailed = "Failed to update data."
    public static let deleteFailed = "Failed to delete data."
    public static let existData = "Current data has been saved."
    public static let notFound = "Nothing was retrieved from the keychain."
}


