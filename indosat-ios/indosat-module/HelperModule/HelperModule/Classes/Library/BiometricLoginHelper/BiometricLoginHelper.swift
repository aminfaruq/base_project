//
//  BiometricLoginHelper.swift
//  SmartLoginModule
//
//  Created by Irfan Handoko on 26/04/21.
//

import Foundation

public class BiometricLoginHelper: NSObject {
    
    public static func biometricType(completion: ((_ data: [String: Any?])->())? = nil) {
        BiometricLoginWorker.biometricType(callback: { (response) in
            completion?(response?.toMap() ?? [:])
        })
    }
    
    public static func authenticateUser(completion: ((_ data: [String: Any?])->())? = nil) {
        BiometricLoginWorker.authenticateUser { (response) in
            completion?(response?.toMap() ?? [:])
        }
    }
    
    public static func saveUserData(key: String?, userData: [String: Any], completion: ((_ data: [String: Any?])->())? = nil) {
        let dataString = GITSStringHelper.convertDictionaryToJSONString(data: userData)
        BiometricLoginWorker.keychainCallback(type: .save, key: (key ?? "") as NSString, data: dataString as NSString) { (response) in
            completion?(response?.toMap() ?? [:])
        }
    }
    
    public static func loadUserData(key: String?, completion: ((_ data: [String: Any?])->())? = nil) {
        BiometricLoginWorker.keychainCallback(type: .load, key: (key ?? "") as NSString) { (response) in
            completion?(response?.toMap() ?? [:])
        }
    }
}
