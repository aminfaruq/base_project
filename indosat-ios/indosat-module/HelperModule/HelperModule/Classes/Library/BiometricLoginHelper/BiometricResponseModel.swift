//
//  ResponseModel.swift
//  BiometricLoginApp
//
//  Created by IRFAN TRIHANDOKO on 07/12/20.
//

import Foundation

public struct BiometricBaseModel {
    public var code: Int?
    public var message: String?
    
    public init(code: Int? = nil, message: String? = nil) {
        self.code = code
        self.message = message
    }
    
    public func toMap() -> [String: Any?] {
        return ["code": self.code ?? 0,
                "message": self.message ?? ""
        ]
    }
}

public struct BiometricTypeModel {
    public var base: BiometricBaseModel?
    public var biometricType: String?
    
    public init(base: BiometricBaseModel? = nil, biometricType: String? = nil) {
        self.base = base
        self.biometricType = biometricType
    }
    
    public func toMap() -> [String: Any?] {
        return ["code": self.base?.code ?? 0,
                "message": self.base?.message ?? "",
                "biometric_type": self.biometricType ?? ""
        ]
    }
}

public struct BiometricUserModel {
    public var base: BiometricBaseModel?
    public var data: [String: Any]?
    
    public init(base: BiometricBaseModel? = nil, data: [String: Any]? = nil) {
        self.base = base
        self.data = data
    }
    
    public func toMap() -> [String: Any?] {
        if self.data == nil {
            return ["code": self.base?.code ?? 0,
                    "message": self.base?.message ?? ""
            ]
        } else {
            return ["code": self.base?.code ?? 0,
                    "message": self.base?.message ?? "",
                    "data": self.data ?? [:]
            ]
        }
    }
}
