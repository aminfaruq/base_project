//
//  UIColor+Ext.swift
//  GITSFramework
//
//  Created by Ajie Pramono Arganata on 11/09/18.
//  Copyright © 2018 GITS Indonesia. All rights reserved.
//

import UIColor_Hex_Swift

extension UIColor {
    public static let colorSecondary = UIColor.yellow
    public static let colorTextPrimary = UIColor.black
    public static let colorTextSecond = UIColor.colorBlackPrimary.withAlphaComponent(0.5)
    
    public static let colorPrimary = UIColor("#19A788")
    public static let colorDarkGray = UIColor("#858A93")
    public static let colorLightGray = UIColor("#B8BCC6")
    
    public static let colorBlackPrimary = UIColor("#222222")
    public static let colorBlackSecond = UIColor.black.withAlphaComponent(0.3)
    
    public static func colorGradientTopBottom(frame: CGRect)-> UIColor {
        return UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [
            UIColor.flatSkyBlue() ?? .lightGray,
            UIColor.flatSkyBlueColorDark() ?? .gray
        ])
    }
    
}

