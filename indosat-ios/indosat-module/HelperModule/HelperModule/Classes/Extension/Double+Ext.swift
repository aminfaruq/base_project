//
//  Double+Ext.swift
//  HelperModule
//
//  Created by IHsan HUsnul on 19/02/21.
//

import Foundation

extension Double {
    public func currency(isDecimal: Bool = false, digit: Int = 0) -> String {
        return GITSStringHelper.currencyFormatting(value: self,
                                                   isUsingDecimal: isDecimal,
                                                   digit: digit)
    }
}
