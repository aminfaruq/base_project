//
//  UILabel+Ext.swift
//  GITSFramework
//
//  Created by Ajie Pramono Arganata on 12/05/20.
//

import UIKit

public extension UILabel {
    static func customLabel(text: String, line: Int, font: UIFont?, color: UIColor, alignment: NSTextAlignment)-> UILabel {
        let label = UILabel(frame: .zero)
        label.text = text
        label.numberOfLines = line
        label.font = font
        label.textColor = color
        label.textAlignment = alignment
        return label
    }
    
    static func add(stringList: [String],
                    font: UIFont,
                    bullet: String = "\u{2022}",
                    indentation: CGFloat = 8,
                    lineSpacing: CGFloat = 2,
                    paragraphSpacing: CGFloat = 4,
                    textColor: UIColor = .gray,
                    bulletColor: UIColor = .gray) -> NSAttributedString {
        
        let textAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: textColor]
        let bulletAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: bulletColor]
        
        let paragraphStyle = NSMutableParagraphStyle()
        let nonOptions = [NSTextTab.OptionKey: Any]()
        paragraphStyle.tabStops = [
            NSTextTab(textAlignment: .left, location: indentation, options: nonOptions)]
        paragraphStyle.defaultTabInterval = indentation
        //paragraphStyle.firstLineHeadIndent = 0
        //paragraphStyle.headIndent = 20
        //paragraphStyle.tailIndent = 1
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.paragraphSpacing = paragraphSpacing
        paragraphStyle.headIndent = indentation
        
        let bulletList = NSMutableAttributedString()
        for string in stringList {
            let formattedString = "\(bullet)\t\(string)\n"
            let attributedString = NSMutableAttributedString(string: formattedString)
            
            attributedString.addAttributes(
                [NSAttributedString.Key.paragraphStyle : paragraphStyle],
                range: NSMakeRange(0, attributedString.length))
            
            attributedString.addAttributes(
                textAttributes,
                range: NSMakeRange(0, attributedString.length))
            
            let string:NSString = NSString(string: formattedString)
            let rangeForBullet:NSRange = string.range(of: bullet)
            attributedString.addAttributes(bulletAttributes, range: rangeForBullet)
            bulletList.append(attributedString)
        }
        
        return bulletList
    }
}
