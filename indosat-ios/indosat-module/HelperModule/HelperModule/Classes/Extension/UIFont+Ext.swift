//
//  UIFont+Ext.swift
//  Alamofire
//
//  Created by Ajie Pramono Arganata on 12/05/20.
//

import UIKit

public extension UIFont {
    enum Poppins: String {
        case black = "Poppins-Black"
        case blackItalic = "Poppins-BlackItalic"
        case bold = "Poppins-Bold"
        case boldItalic = "Poppins-BoldItalic"
        case extraBold = "Poppins-ExtraBold"
        case extraBoldItalic = "Poppins-ExtraBoldItalic"
        case extraLight = "Poppins-ExtraLight"
        case extraLightItalic = "Poppins-ExtraLightItalic"
        case italic = "Poppins-Italic"
        case light = "Poppins-Light"
        case lightItalic = "Poppins-LightItalic"
        case medium = "Poppins-Medium"
        case mediumItalic = "Poppins-MediumItalic"
        case regular = "Poppins-Regular"
        case semiBold = "Poppins-SemiBold"
        case semiBoldItalic = "Poppins-SemiBoldItalic"
        case thin = "Poppins-Thin"
        case thinItalic = "Poppins-ThinItalic"
        
        public func font(ofSize: CGFloat)-> UIFont {
            return UIFont(name: self.rawValue, size: ofSize) ?? UIFont.systemFont(ofSize: ofSize)
        }
    }
    
    static func loadCustomFont() {
        self.jbsRegisterFont(withFilename: Poppins.black)
        self.jbsRegisterFont(withFilename: Poppins.blackItalic)
        self.jbsRegisterFont(withFilename: Poppins.bold)
        self.jbsRegisterFont(withFilename: Poppins.boldItalic)
        self.jbsRegisterFont(withFilename: Poppins.extraBold)
        self.jbsRegisterFont(withFilename: Poppins.extraBoldItalic)
        self.jbsRegisterFont(withFilename: Poppins.extraLight)
        self.jbsRegisterFont(withFilename: Poppins.extraLightItalic)
        self.jbsRegisterFont(withFilename: Poppins.italic)
        self.jbsRegisterFont(withFilename: Poppins.light)
        self.jbsRegisterFont(withFilename: Poppins.lightItalic)
        self.jbsRegisterFont(withFilename: Poppins.medium)
        self.jbsRegisterFont(withFilename: Poppins.mediumItalic)
        self.jbsRegisterFont(withFilename: Poppins.regular)
        self.jbsRegisterFont(withFilename: Poppins.semiBold)
        self.jbsRegisterFont(withFilename: Poppins.semiBoldItalic)
        self.jbsRegisterFont(withFilename: Poppins.thin)
        self.jbsRegisterFont(withFilename: Poppins.thinItalic)
    }
    
    private static func jbsRegisterFont(withFilename filename: Poppins) {
        let bundleURL = HelperBundle.bundle?.url(forResource: HelperBundle.moduleName, withExtension: "bundle")
        guard let url = bundleURL, let bundle = Bundle(url: url) else {
            print("Faild load bundle")
            return }
        guard let pathForResourceString = bundle.path(forResource: filename.rawValue, ofType: "ttf") else {
            print("UIFont+:  Failed to register font - path for resource not found.")
            return
        }

        guard let fontData = NSData(contentsOfFile: pathForResourceString) else {
            print("UIFont+:  Failed to register font - font data could not be loaded.")
            return
        }

        guard let dataProvider = CGDataProvider(data: fontData) else {
            print("UIFont+:  Failed to register font - data provider could not be loaded.")
            return
        }

        guard let font = CGFont(dataProvider) else {
            print("UIFont+:  Failed to register font - font could not be loaded.")
            return
        }

        var errorRef: Unmanaged<CFError>? = nil
        if (CTFontManagerRegisterGraphicsFont(font, &errorRef) == false) {
            print("UIFont+:  Failed to register font - register graphics font failed - this font may have already been registered in the main bundle.")
        }
    }

}
