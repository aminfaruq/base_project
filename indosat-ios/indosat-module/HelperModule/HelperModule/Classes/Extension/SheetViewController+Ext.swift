//
//  SheetViewController+Ext.swift
//  HelperModule
//
//  Created by Intan Nurjanah on 21/05/21.
//  Copyright © 2021 GITS Indonesia. All rights reserved.
//

import Foundation
import FittedSheets

private let swizzling: (AnyClass, Selector, Selector) -> () = { forClass, originalSelector, swizzledSelector in
    guard
        let originalMethod = class_getInstanceMethod(forClass, originalSelector),
        let swizzledMethod = class_getInstanceMethod(forClass, swizzledSelector)
    else { return }
    method_exchangeImplementations(originalMethod, swizzledMethod)
}

extension SheetViewController {
    
    public static let classInit: Void = {
        let originalViewDidAppearSelector = #selector(viewDidAppear(_:))
        let swizzledViewDidAppearSelector = #selector(swizzled_viewDidAppear(_:))
        swizzling(SheetViewController.self, originalViewDidAppearSelector, swizzledViewDidAppearSelector)
        
        let originalViewWillDisappearSelector = #selector(viewWillDisappear(_:))
        let swizzledViewWillDisappearSelector = #selector(swizzled_dismissTapped(_:))
        swizzling(SheetViewController.self, originalViewWillDisappearSelector, swizzledViewWillDisappearSelector)
    }()
    
    @objc func swizzled_viewDidAppear(_ animated: Bool) {
        show()
    }
    
    @objc func swizzled_dismissTapped(_ animated: Bool) {
        dismissSheet()
        swizzled_dismissTapped(animated)
    }
    
    public func show() {
        self.view.alpha = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.alpha = 0.66
        })
    }
    
    func dismissSheet() {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.alpha = 0
        })
        
    }
    
    
}


