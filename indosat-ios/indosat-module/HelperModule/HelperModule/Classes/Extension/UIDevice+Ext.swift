//
//  UIDevice+Ext.swift
//  HelperModule
//
//  Created by Irfan Handoko on 30/04/21.
//

import Foundation

extension UIDevice {
    public static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        if identifier == "i386" || identifier == "x86_64" {
            return "Simulator\(ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS")".replacingOccurrences(of: " ", with: "")
        }
        return identifier.replacingOccurrences(of: " ", with: "")
    }()
}
