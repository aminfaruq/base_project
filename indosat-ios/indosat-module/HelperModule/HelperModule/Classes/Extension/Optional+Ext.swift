//
//  Optional+Ext.swift
//  HelperModule
//
//  Created by IHsan HUsnul on 10/03/21.
//

import Foundation

public extension Optional where Wrapped == String {
    
    var isEmptyOrNil: Bool {
        return self?.isEmpty ?? true
    }
}
