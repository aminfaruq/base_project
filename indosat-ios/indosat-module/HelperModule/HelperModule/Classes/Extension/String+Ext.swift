//
//  String+Extension.swift
//  HelperModule
//
//  Created by Ajie Pramono Arganata on 18/09/20.
//  Copyright © 2020 Ajie Arganata. All rights reserved.
//

import UIKit

public extension String {
    
    /** Estimate height of a string.
        https://stackoverflow.com/questions/30450434/figure-out-size-of-uilabel-based-on-string-in-swift
     */
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)

        return ceil(boundingBox.height)
    }

    /** Estimate width of a string.
       https://stackoverflow.com/questions/30450434/figure-out-size-of-uilabel-based-on-string-in-swift
    */
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)

        return ceil(boundingBox.width)
    }
}
