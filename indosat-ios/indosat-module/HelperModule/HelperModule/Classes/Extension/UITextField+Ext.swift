//
//  TextField+Ext.swift
//  GITSFramework
//
//  Created by Ajie Pramono Arganata on 16/01/20.
//  Copyright © 2020 GITS Indonesia. All rights reserved.
//

import Material
import PureLayout

/// A Custom Error TF if using leftview
public class CustomErrorTextField: ErrorTextField {
    private var cornerRadius: CGFloat = 6.0 //default radius
    private var borderWidth: CGFloat = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    public var textInsetsRight: CGFloat = 0
    public var textInsetsLeft: CGFloat = 0
    public var textInsetsTop: CGFloat = 6
    private var shadow: CGFloat = 1 //default
    private var shadowRadius: CGFloat = 2.0
    public var iconLeftImageView: UIImageView?
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.dividerContentEdgeInsets.left = 0
        self.textInsets.left = self.textInsetsLeft
        self.textInsets.right = self.textInsetsRight
        if self.leftView != nil {
            self.textInsets.left = 13
            self.leftViewOffset = -8
        }
        let leftPadding = leftViewWidth + textInsetsLeft
        
        let w = bounds.width - leftPadding - (rightView?.bounds.size.width ?? 0)
        placeholderLabel.bounds.size = CGSize(width: w, height: placeholderLabel.bounds.size.height)
        
        guard isEditing || !isEmpty || !isPlaceholderAnimated else {
            placeholderLabel.transform = CGAffineTransform.identity
            placeholderLabel.frame.origin = CGPoint(x: leftPadding, y: textInsetsTop)
          return
        }
        
        placeholderLabel.transform = CGAffineTransform(scaleX: placeholderActiveScale, y: placeholderActiveScale)
        placeholderLabel.frame.origin.y = -placeholderLabel.frame.height + placeholderVerticalOffset
        
        switch placeholderLabel.textAlignment {
        case .left, .natural:
          placeholderLabel.frame.origin.x = leftPadding + placeholderHorizontalOffset
        case .right:
          let scaledWidth = w * placeholderActiveScale
          placeholderLabel.frame.origin.x = bounds.width - scaledWidth - textInsets.right + placeholderHorizontalOffset
        default:break
        }
        
        self.errorLabel.frame.origin.x = 0
    }
    
    public func applyStyleBorder(shadow: CGFloat = 1, borderColor: UIColor? = nil, borderWidth: CGFloat = 0, borderRadius: CGFloat = 0) {
        self.shadow = shadow
        self.borderWidth = borderWidth
        self.layer.borderWidth = self.borderWidth
        self.borderColor = borderColor
        self.layer.borderColor = self.borderColor?.cgColor
        self.cornerRadius = borderRadius
        self.layer.cornerRadius = self.cornerRadius
    }
}

extension CustomErrorTextField {
    public func setLeftIcon(_ icon: UIImage, _ height: Int) {
        leftView?.removeFromSuperview()
        leftView = nil
        let padding = 8
        let size = 16
        
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: size+padding, height: size) )
        self.iconLeftImageView  = UIImageView(frame: CGRect(x: padding, y: (height/4)+4, width: size, height: size))
        self.iconLeftImageView?.image = icon
        self.iconLeftImageView?.contentMode = .scaleAspectFit
        outerView.addSubview(self.iconLeftImageView!)
        
        leftView = outerView
        leftViewMode = .always
    }
    
    public func setLeftIcon(_ icon: UIImage, _ height: Int, width: Int) {
        leftView?.removeFromSuperview()
        leftView = nil
        let padding = 8
        
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: width+padding, height: width) )
        self.iconLeftImageView  = UIImageView(frame: CGRect(x: padding, y: (height/2)-(width/2), width: width, height: width))
        self.iconLeftImageView?.image = icon
        self.iconLeftImageView?.contentMode = .scaleAspectFit
        outerView.addSubview(iconLeftImageView!)
        leftView = outerView
        leftViewMode = .always
    }

    public func appyStyle(placeholder: String, isClearIconButtonEnabled: Bool = false, isVisibilityIconButtonEnabled: Bool = false, fontText: UIFont?, fontError: UIFont?) {
        self.placeholder = placeholder
        self.isClearIconButtonEnabled = isClearIconButtonEnabled
        self.isVisibilityIconButtonEnabled = isVisibilityIconButtonEnabled
        self.placeholderLabel.backgroundColor = .clear
        font = fontText
        errorLabel.font = fontError
        detailLabel.font = fontError
        errorVerticalOffset = 4
        detailVerticalOffset = 4
        placeholderVerticalOffset = 10
        dividerActiveColor = UIColor.colorPrimary
        dividerNormalColor = UIColor.black.withAlphaComponent(0.3)
        placeholderActiveColor = UIColor.colorPrimary
        placeholderNormalColor = UIColor.black.withAlphaComponent(0.3)
        self.autocorrectionType = .no
        if #available(iOS 11.0, *) {
            self.smartDashesType = .no
            self.smartQuotesType = .no
            self.smartInsertDeleteType = .no
        }
        self.spellCheckingType = .no
        
    }
    
    public func isTextNotEmpty(errMessage: String, isShowError: Bool = true, priceInput: Bool = false)-> Bool {
        error = errMessage
        let result = GITSStringHelper.isTextNotEmpty(text: text ?? "", priceInput: priceInput)
        if isShowError {
            isErrorRevealed = !result
        }
        return result
    }
     
    public func isLengthTextMinMax(min: Int, max: Int, errMessage: String, isShowError: Bool = true, isPhoneNum: Bool = false)-> Bool {
        error = errMessage
        let result = GITSStringHelper.isLengthTextMinMax(text: text ?? "", min: min, max: max, isPhoneNum: isPhoneNum)
        if isShowError {
            isErrorRevealed = !result
        }
        return result
    }
     
    public func compareText(withText: String, errMessage: String, isShowError: Bool = true)-> Bool {
        error = errMessage
        let result = GITSStringHelper.compareText(text: text ?? "", withText: withText)
        if isShowError {
            isErrorRevealed = !result
        }
        return result
    }
    
    public func isTextEmailRegex(errMessage: String, isShowError: Bool = true)->Bool {
        error = errMessage
        let result = GITSStringHelper.isTextEmailRegex(text: text ?? "")
        if isShowError {
            isErrorRevealed = !result
        }
        return result
    }
     
    public func textShouldChangeMaxInput(max: Int, range: NSRange, newString: String, isPhoneNum: Bool = false)-> Bool {
        return GITSStringHelper.textMaxInput(text: text, max: max, range: range, newString: newString, isPhoneNum: isPhoneNum)
    }
    
    public func textShouldChangeRegexOnlyNum(newString: String, isPhoneNum: Bool = false, range: NSRange)-> Bool {
        return GITSStringHelper.textRegexOnlyNum(newString: newString, isPhoneNum: isPhoneNum, range: range)
    }
}

extension UITextField {
    public func addNextButtonAccessoryView() {
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.resignFirstResponder))
        let flexSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
        let toolbar = UIToolbar()
        toolbar.barStyle = UIBarStyle.default
        toolbar.isTranslucent = true
        toolbar.tintColor = UIColor.blue
        toolbar.sizeToFit()
        toolbar.setItems([flexSpace, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        self.inputAccessoryView = toolbar
    }
}
