//
//  NavigationWireframe.swift
//  NavigationWireframe
//
//  Created by GITS INDONESIA on 25/11/20.
//  Copyright © 2020 GITS INDONESIA. All rights reserved.
//

import HelperModule
import SampleModule


public class NavigationWireframe: NavigationDelegate {
    
    
    public var output: NavigationOutputDelegate?
    public init() {}
    
    
    public func initToSample() -> UIViewController {
        return SampleWireframe.initToSample()
    }
}
