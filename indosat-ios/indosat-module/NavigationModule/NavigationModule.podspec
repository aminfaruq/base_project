Pod::Spec.new do |s|
s.name         = "NavigationModule"
s.version      = "1.0.0"
s.summary      = "A Module that used to connect all module"
s.homepage     = "https://gitlab.com/aminfaruq/base_project"
s.license      = "MIT (ios)"
s.author             = { "Amin Faruq" => "aminfaruk.fa@gmail.com" }
    s.source       = { :git => "https://gitlab.com/aminfaruq/base_project.git", :tag => "#{s.version}" }
s.source_files  = "NavigationModule/Classes/**/*.{h,m,swift}"
s.resource_bundles = {'NavigationModule' => ['NavigationModule/Assets/**/*.{storyboard,xib,xcassets,json,imageset,png,plist}']}
s.platform         = :ios, "10.0"
s.dependency 'HelperModule'
s.dependency 'NetworkModule'
s.dependency 'SampleModule'

end
