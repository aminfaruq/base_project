//
//  BaseBody.swift
//  GITSNetwork
//
//  Created by Ajie Pramono Arganata on 09/01/20.
//  Copyright © 2020 GITS INDONESIA. All rights reserved.
//

import Alamofire

public protocol BodyCodable {
    func asFormDictionary() -> [String: Any]
}

open class BaseBody {
    open var param: [String: Any] = [:]
    
    func asFormData(ext: String?=nil)-> (MultipartFormData)-> Void {
        return { formData in
            for (key, value) in self.param {
                if let value = value as? Data {
                    let ext = ext != nil ? "pdf" : "jpg"
                    formData.append(value, withName: key, fileName: "image.\(ext)", mimeType: ext == "pdf" ? "application/pdf" : "image/jpeg")
                } else {
                    formData.append("\(value)".data(using: .utf8)!, withName: key)
                }
            }
        }
    }
}

public extension BodyCodable where Self: Codable {
    func asFormDictionary() -> [String: Any] {
        guard let data = try? JSONEncoder().encode(self),
              let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else {
            return [:]
        }
        return dict
    }
}
