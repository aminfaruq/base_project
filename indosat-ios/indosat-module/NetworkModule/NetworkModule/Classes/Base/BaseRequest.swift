//
//  BaseRequest.swift
//  GITSFramework
//
//  Created by Ajie Pramono Arganata on 02/09/19.
//  Copyright © 2019 GITS Indonesia. All rights reserved.
//

import Alamofire
import SwiftyJSON
import HelperModule

public typealias onRequest = (_ request: DataRequest)->()
public typealias onSuccess = (_ result: Any)->()
public typealias onFailed = (_ code: Int?, _ message: String)->()

struct BaseRequest {
    static func request<T:Codable>(url: ApiService, usingMultipart: Bool = false, onSuccess: @escaping (T)->(), onFailed: @escaping onFailed, onRequest: @escaping onRequest, onProgress: ((_ progress: Double)->())? = nil) {
        var dataRequest: DataRequest?
        if usingMultipart, let data = url.multipartData() {
            dataRequest = AF.upload(multipartFormData: data, with: url)
        } else {
            dataRequest = AF.request(url)
        }
        guard let request = dataRequest else {
            onFailed(nil, "No request!")
            return
        }
        request.uploadProgress { (progress) in
            print("Progress api \(progress.fractionCompleted)")
            onProgress?(progress.fractionCompleted)
        }
        request.responseDecodable(of: T.self) { (response) in
            let errorMessage = response.error?.errorDescription ?? ""
            let code = response.error?.responseCode == nil ? response.response?.statusCode : response.error?.responseCode
            if let data = response.value {
                if errorMessage.contains("offline") {
                    onFailed(code, "network_not_any_internet".localized(lang: GITSPref.getLanguage()))
                } else {
                    if code == 404 {
                        onFailed(code, "network_not_found".localized(lang: GITSPref.getLanguage()))
                    } else if code == 503 {
                        onFailed(code, "network_server_error".localized(lang: GITSPref.getLanguage()))
                    } else {
                        let baseDao = BaseDAO(response: data)
                        baseDao.printJSON()
                        onSuccess(data)
                    }
                }
            } else {
                if code == 404 {
                    onFailed(code, "network_not_found".localized(lang: GITSPref.getLanguage()))
                } else if code == 503 {
                    onFailed(code, "network_server_error".localized(lang: GITSPref.getLanguage()))
                } else {
                    onFailed(code, response.error?.errorDescription ?? "network_server_error".localized(lang: GITSPref.getLanguage()))
                }
            }
        }
        onRequest(request)
    }
    
    private static func unauthorized() {
        if GITSPref.getString(key: GITSPref.keyAccessToken) != "" {
            GITSPref.remove(key: GITSPref.keyAccessToken)
            GITSPref.remove(key: GITSPref.keyAccessRefreshToken)
            GITSPref.remove(key: GITSPref.keyEmailVerified)
            GITSPref.remove(key: GITSPref.keySocmedLogin)
            GITSPref.remove(key: GITSPref.keyEmail)
            GITSPref.remove(key: GITSPref.phoneNumber)
            GITSPref.remove(key: GITSPref.keyNeedEmailVerification)
            GITSPref.remove(key: GITSPref.keyFullname)
            GITSPref.remove(key: GITSPref.keyIsSmartLogin)
            GITSPref.remove(key: GITSPref.keyFirebaseToken)
            GITSPref.remove(key: GITSPref.keyFirebaseRegistered)
        }
    }
}

class BaseDAO<T:Encodable>: NSObject {
    var response: T?
    
    init (response: T) {
        self.response = response
    }
    
    func printJSON() {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        do {
            let jsonData = try jsonEncoder.encode(response)
            let json = String(data: jsonData, encoding: .utf8)
            print("JSON Result =>")
            print(json ?? "")
        } catch {
            print(error.localizedDescription)
        }
    }
}
