//
//  ApiConstant.swift
//  GITSNetwork
//
//  Created by Ajie Pramono Arganata on 09/01/20.
//  Copyright © 2020 GITS INDONESIA. All rights reserved.
//

import Foundation

/// This is a struct of Api Constant
public struct ApiConstant {
    // MARK: API URL
    public static let urlApi = GITSApps.GetUrl()
}
