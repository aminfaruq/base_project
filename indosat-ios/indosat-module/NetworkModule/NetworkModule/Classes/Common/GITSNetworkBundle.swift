//
//  GITSNetworkBundle.swift
//  Pods
//
//  Created by Ajie Pramono Arganata on 14/02/20.
//

import Foundation

class GITSNetworkBundle {
    static var bundle: Bundle {
        let podBundle = Bundle(for: GITSNetworkBundle.self)
        let bundleURL = podBundle.url(forResource: "NetworkModule", withExtension: "bundle")
        if bundleURL == nil {
            return podBundle
        }else{
            return Bundle(url: bundleURL!)!
        }
    }
}
