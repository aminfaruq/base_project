//
//  GITSApps.swift
//  GITSFramework
//
//  Created by GITS Indonesia on 3/15/17.
//  Copyright © 2017 GITS Indonesia. All rights reserved.
//

import Foundation


/// GITSApps is an class for take the network config plist
public struct GITSApps {
    public static func GetConfig() -> Dictionary<String, Any>? {
        var myDict: Dictionary<String, Any>?
        if let path = GITSNetworkBundle.bundle.path(forResource: "NetworkConfig", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: Any] {
            myDict = dict
        }
        
        if let url = GITSNetworkBundle.bundle.url(forResource: "NetworkConfig", withExtension: "plist") {
            let _ = NSDictionary(contentsOf: url)
        }
        
        if let fileUrl = GITSNetworkBundle.bundle.url(forResource: "NetworkConfig", withExtension: "plist"),
            let data = try? Data(contentsOf: fileUrl) {
            if let _ = try? PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [[String: Any]] {}
        }
        return myDict
    }
    
    public static func GetUrl() -> String{
        if let config = GetConfig() {
            if config["is_prod"] as! Bool {
                return config["base_url"] as! String
            } else {
                return config["dev_base_url"] as! String
            }
        }
        return ""
    }
    
    public static func GetUrlImage() -> String{
        if let config = GetConfig() {
            if config["is_prod"] as! Bool {
                return config["image_url"] as! String
            } else {
                return config["dev_image_url"] as! String
            }
        }
        return ""
    }
    
    public static func getIsProd() -> Bool {
        if let config = GetConfig() {
            return config["is_prod"] as! Bool
        }
        return false
    }
    
    public static func GetApiKey() -> String{
        if let config = GetConfig() {
            return config["api_key"] as? String ?? ""
        }
        return ""
    }
}
