//
//  ApiService.swift
//  GITSNetwork
//
//  Created by Ajie Pramono Arganata on 02/09/19.
//  Copyright © 2019 GITS Indonesia. All rights reserved.
//

import Alamofire

/// Api Service is an enum to implement the networking api
enum ApiService: URLRequestConvertible {
    // MARK: Api URL
    static let baseURLString = ApiConstant.urlApi
    // MARK: Google
    case doGetData
    case doUploadData
    
    
    var method: HTTPMethod {
        switch self {
        case .doGetData:
            return .get
        default:
            return .post
        }
    }
    
    var res: (path: String, param: [String: Any]) {
        switch self {
        // Google
        case .doGetData:
            return ("", ["": ""])
        default:
            return ("", ["": ""])
        }
    }
    
    func multipartData()->((MultipartFormData)-> Void)? {
        return nil
    }
    
    func asURLRequest() throws -> URLRequest {
        var url: URL?
        var urlRequest: URLRequest?
        
        url = try (ApiService.baseURLString+res.path).asURL()
        
        urlRequest = URLRequest(url: url!)
        urlRequest?.httpMethod = method.rawValue
        urlRequest = try multipartData() != nil ? URLEncoding.default.encode(urlRequest!, with: res.param) : method == .get ? URLEncoding.default.encode(urlRequest!, with: res.param) : JSONEncoding.default.encode(urlRequest!, with: res.param)
        
        let urlString = urlRequest?.url?.absoluteString.replacingOccurrences(of: "%3F", with: "?").replacingOccurrences(of: "%2C", with: ",").replacingOccurrences(of: "%3A", with: ":")
        urlRequest?.addValue(GITSApps.GetApiKey(), forHTTPHeaderField: "api_key")
        
        // DEFINE API NOT USING AUTHORIZATION
        switch self {
        case .doUploadData:
            urlRequest?.addValue("Bearer " + GITSPref.getString(key: GITSPref.keyAccessRefreshToken), forHTTPHeaderField: "Authorization")
        default:
            urlRequest?.addValue("Bearer " + GITSPref.getString(key: GITSPref.keyAccessToken), forHTTPHeaderField: "Authorization")
        }
        
        urlRequest?.url = URL(string: urlString!)
        print("\n==============================================>")
        print("HEADER  => \(String(describing: urlRequest?.allHTTPHeaderFields))")
        print("URL API => "+(urlRequest?.url?.absoluteString)!)
        print("Parameter => \(res.param)")
        print("IS Multipart \(multipartData() != nil)")
        return urlRequest!
    }
}
