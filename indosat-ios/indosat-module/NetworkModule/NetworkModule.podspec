Pod::Spec.new do |s|
  s.name         = "NetworkModule"
  s.version      = "1.0.0"
  s.summary      = "Network Module."
  s.homepage     = "https://gitlab.com/aminfaruq/base_project"
  s.license      = "MIT (ios)"
  s.author             = { "Amin Faruq" => "aminfaruk.fa@gmail.com" }
  s.source       = { :git => "https://gitlab.com/aminfaruq/base_project.git", :tag => "#{s.version}" }
  s.source_files  = "NetworkModule/Classes/**/*.{h,m,swift,map}"
  s.resource_bundles = {'NetworkModule' => ['NetworkModule/Assets/**/*.{storyboard,xib,xcassets,json,imageset,png,plist}']}
  s.platform         = :ios, "10.0"
  s.dependency 'Alamofire'
  s.dependency 'SwiftyJSON'
  s.dependency 'HelperModule' 
  s.swift_version = '5.0'
end
