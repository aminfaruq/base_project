//
//  SampleWireframe.swift
//  SampleModule
//
//  Created by Irfan Handoko on 22/06/21.
//

import UIKit
import HelperModule

public struct SampleWireframe {
    
    public static func initToSample()-> UIViewController {
        let vc = UIStoryboard(name: "Sample", bundle: bundle).instantiateInitialViewController() as? SampleVC ?? UIViewController()
        vc.title = ""
        let nav = UINavigationController(rootViewController: vc)
        return nav
    }
    
    public static func performToSampleList(caller: UIViewController, data: [String: Any?]) {
        let vc = UIStoryboard(name: "SampleList", bundle: bundle).instantiateInitialViewController() as? SampleListVC ?? UIViewController()
        vc.title = "Sample List"
        vc.hidesBottomBarWhenPushed = true
        vc.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        caller.navigationController?.pushViewController(vc, animated: true)
    }
    
    public static func performToSampleMultiList(caller: UIViewController, data: [String: Any?]) {
        let vc = UIStoryboard(name: "SampleMultiList", bundle: bundle).instantiateInitialViewController() as? SampleMultiListVC ?? UIViewController()
        vc.title = "Multi List"
        vc.hidesBottomBarWhenPushed = true
        vc.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        caller.navigationController?.pushViewController(vc, animated: true)
    }
    
    static var bundle: Bundle {
        let podBundle = Bundle(for: SampleVC.self)
        let bundleURL = podBundle.url(forResource: "SampleModule", withExtension: "bundle")
        if bundleURL == nil {
            return podBundle
        }else{
            return Bundle(url: bundleURL!)!
        }
    }
}
