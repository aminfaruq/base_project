//
//  SampleMultiListSectionController.swift
//  SampleModule
//
//  Created by Irfan Handoko on 28/06/21.
//

import Foundation
import HelperModule

class SampleMultiListSectionController: BaseListSectionController {
    // List Items
    var items: [MultiListModel] = []
    var onSelectItem: ((_ section: Int, _ idx: Int)->())?
    
    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.minimumInteritemSpacing = 0
        self.minimumLineSpacing = 0
        self.onRefreshDataSection = {
            self.collectionContext?.performBatch(animated: true, updates: { (batchContext) in
                batchContext.reload(self)
            }, completion: nil)
        }
    }
    
    override func numberOfItems() -> Int {
        return self.items.count
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        if self.items[index].index == 0 {
            return self.cellForHeaderItem(at: index)
        } else {
            return self.cellForContentItem(at: index)
        }
    }
    
    private func cellForHeaderItem(at index: Int) -> UICollectionViewCell {
        let cell = SampleListCell.initCell(self.collectionContext, section: self, index: index, bundle: SampleWireframe.bundle, item: items[index].title) as! SampleListCell
        cell.configureView()
        return cell
    }
    
    private func cellForContentItem(at index: Int) -> UICollectionViewCell {
        let cell = SampleMultiListCell.initCell(self.collectionContext, section: self, index: index, bundle: SampleWireframe.bundle, item: items[index].title) as! SampleMultiListCell
        cell.configureView()
        cell.setButtonHandler { (view) in
            self.onSelectedItem?(self.items[index - 1].section ?? 0, self.items[index - 1].index ?? 0)
        }
        return cell
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        if self.items[index].index == 0 {
            return SampleListCell.cellSize(screenWidth: self.collectionContext?.containerSize.width ?? 0, item: items[index].title)
        } else {
            return SampleMultiListCell.cellSize(screenWidth: self.collectionContext?.containerSize.width ?? 0, item: items[index].title)
        }
    }
}



