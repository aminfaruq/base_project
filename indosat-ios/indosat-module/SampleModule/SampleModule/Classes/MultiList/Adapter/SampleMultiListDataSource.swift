//
//  SampleMultiListDataSource.swift
//  SampleModule
//
//  Created by Irfan Handoko on 28/06/21.
//

import UIKit
import IGListKit
import HelperModule

class SampleMultiListDataSource: NSObject, ListAdapterDataSource {
    /// Section Sample Multi List Item
    var sectionMultiList = SampleMultiListSectionController()
    /// Section  Load More
    private var sectionLoadMore = LoadMoreSectionController()
    
    func sectionSelectedItem(onSelectedItem: ((_ section: Int, _ idx: Int)->())?) {
        self.sectionMultiList.onSelectedItem = onSelectedItem
    }
    
    func addSampleMultiList(items: [MultiListModel]) {
        self.sectionMultiList.items = items
        self.sectionMultiList.onRefreshDataSection?()
    }

    func getTotalMultiList() -> Int {
        return self.sectionMultiList.items.count
    }
    
    func getSampleHeader(index: Int) -> MultiListModel {
        return self.sectionMultiList.items[index]
    }
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return ["list", "loadMore"] as [ListDiffable]
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        let type = object as? String
        if type == "list" {
            return self.sectionMultiList
        } else {
            return self.sectionLoadMore
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}



