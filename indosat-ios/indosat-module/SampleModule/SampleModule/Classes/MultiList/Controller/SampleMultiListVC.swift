//
//  SampleMultiListVC.swift
//  SampleModule
//
//  Created by Irfan Handoko on 28/06/21.
//

import UIKit
import HelperModule
import IGListKit
import SwiftMessages

class SampleMultiListVC: BaseVC {
    
    @IBOutlet weak var collectionVw: UICollectionView?
    
    private lazy var dataSource = SampleMultiListDataSource()
    private lazy var adapter: ListAdapter = {
        return ListAdapter(
            updater: ListAdapterUpdater(),
            viewController: self,
            workingRangeSize: 0)
    }()
    private var viewModel = SampleMultiListVM()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
        self.refresh()
    }
    
    override func configureView() {
        // Configure Adapter
        SampleListCell.registerCell(with: self.collectionVw ?? UICollectionView(), bundle: self.nibBundle)
        SampleMultiListCell.registerCell(with: self.collectionVw ?? UICollectionView(), bundle: self.nibBundle)
        self.adapter.collectionView = self.collectionVw
        self.adapter.dataSource = self.dataSource
        self.dataSource.sectionSelectedItem { section, index in
            debugPrint("Section: \(section) Index: \(index)")
        }
        self.initRefresh(on: self.collectionVw ?? UICollectionView())
        self.observable()
    }

    override func refresh() {
        self.viewModel.loadMultiList()
    }
}


extension SampleMultiListVC {
    
    private func observable() {
        self.viewModel.loadMultiListItem.observe = { [weak self] response in
            guard let self = self else { return }
            self.dataSource.addSampleMultiList(items: response)
        }
        
        // Load Is Still Loading
        self.viewModel.loadApi.observe = { [weak self] isLoad in
            guard let self = self else { return }
            HelperVC.loadNetworkActivityIndicator(visible: isLoad)
            HelperVC.showLoading(view: self.view, isLoad: isLoad)
            if !isLoad {
                self.stopRefresh()
            }
        }
        
        // Load Error Message
        self.viewModel.loadError.observe = { message in
            let option: [HelperVC.MessageOptionKey: Any] = [
                .style: SwiftMessages.PresentationStyle.top
            ]
            HelperVC.showMessage(body: message, state: true, option: option)
        }
    }
    
}

