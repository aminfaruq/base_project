//
//  SampleMultiListVM.swift
//  SampleModule
//
//  Created by Irfan Handoko on 28/06/21.
//

import Foundation
import HelperModule
import NetworkModule

class SampleMultiListVM: BaseVM {
    
    public var loadMultiListItem = Observable<[MultiListModel]>()
    
    public func loadMultiList() {
        self.loadApi.property = true
        
        // Sample Response
        var list: [SampleMultiListResponse] = []
        for i in 0..<5 {
            list.append(SampleMultiListResponse(title: "\(i+1). UB unveils James Joyce mural in downtown Buffalo, plans creation of Joyce museum",
                                                result: [
                                                    "\(i+1).1. The mural, which arrives ahead of the 100th anniversary of Joyce’s landmark book “Ulysses.",
                                                    "\(i+1).2. Part of a broader fundraising campaign and initiative to raise awareness of the UB James Joyce Collection."]
            ))
        }
        
        // Setup To Model
        var section: Int = 0
        var index: Int = 0
        var result: [MultiListModel] = []
        list.forEach { listItem in
            index = 0
            result.append(MultiListModel(title: listItem.title, section: section, index: index))
            listItem.result?.forEach({ resultItem in
                index += 1
                result.append(MultiListModel(title: resultItem, section: section, index: index))
            })
            section += 1
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loadApi.property = false
            self.loadMultiListItem.property = result
        }
    }
}
