//
//  MultiListModel.swift
//  SampleModule
//
//  Created by Irfan Handoko on 28/06/21.
//

import Foundation

class SampleMultiListResponse: NSObject {
    var title: String?
    var result: [String]?
    
    init(title: String? = nil, result: [String]? = nil) {
        self.title = title
        self.result = result
    }
}

class MultiListModel: NSObject {
    
    var title: String?
    var section: Int?
    var index: Int?
    
    init(title: String? = nil, section: Int? = nil, index: Int? = nil) {
        super.init()
        self.title = title
        self.section = section
        self.index = index
    }
}
