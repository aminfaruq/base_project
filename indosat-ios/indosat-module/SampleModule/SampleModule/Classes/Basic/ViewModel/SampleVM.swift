//
//  SampleVM.swift
//  SampleModule
//
//  Created by Irfan Handoko on 22/06/21.
//


import Foundation
import HelperModule
import NetworkModule

class SampleVM: BaseVM {
    
    public var loadGreetingsItem = Observable<String>()
    
    public func loadGreetings() {
        self.loadApi.property = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loadApi.property = false
            self.loadGreetingsItem.property = "Success to Get News List"
        }
    }
}
