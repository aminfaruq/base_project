//
//  SampleVC.swift
//  SampleModule
//
//  Created by Irfan Handoko on 22/06/21.
//

import HelperModule
import UIKit
import SwiftMessages

class SampleVC: BaseVC {
    /// Properties
    @IBOutlet weak var clickBtn: CustomButton?
    /// View Model
    private var viewModel = SampleVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.configColorBar(colorBar: [.white], colorTitle: .black, colorBarButton: .black, alpha: 1, isTranslucent: false)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func configureView() {
        self.clickBtn?.applyStyleBackground(backgroundColor: .colorGradientTopBottom(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width-32, height: 40)))
        self.observable()
    }

    @IBAction func clickAction(_ sender: UIButton) {
        self.viewModel.loadGreetings()
    }
}

extension SampleVC {
    
    private func observable() {
        self.viewModel.loadGreetingsItem.observe = { [weak self] response in
            guard let self = self else { return }
            HelperVC.showMessage(body: response, state: true, option: [:])
            SampleWireframe.performToSampleList(caller: self, data: [:])
        }
        
        // Load Is Still Loading
        self.viewModel.loadApi.observe = { [weak self] isLoad in
            guard let self = self else { return }
            HelperVC.loadNetworkActivityIndicator(visible: isLoad)
            HelperVC.showLoading(view: self.view, isLoad: isLoad)
        }
        
        // Load Error Message
        self.viewModel.loadError.observe = { message in
            let option: [HelperVC.MessageOptionKey: Any] = [
                .style: SwiftMessages.PresentationStyle.top
            ]
            HelperVC.showMessage(body: message, state: true, option: option)
        }
    }
    
}
