//
//  SampleListVC.swift
//  SampleModule
//
//  Created by Irfan Handoko on 23/06/21.
//

import UIKit
import HelperModule
import IGListKit
import SwiftMessages

class SampleListVC: BaseVC {
    
    @IBOutlet weak var collectionVw: UICollectionView?
    
    private lazy var dataSource = SampleListDataSource()
    private lazy var adapter: ListAdapter = {
        return ListAdapter(
            updater: ListAdapterUpdater(),
            viewController: self,
            workingRangeSize: 0)
    }()
    private var viewModel = SampleListVM()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
        self.refresh()
    }
    
    override func configureView() {
        // Configure Adapter
        SampleListCell.registerCell(with: self.collectionVw ?? UICollectionView(), bundle: self.nibBundle)
        self.adapter.collectionView = self.collectionVw
        self.adapter.dataSource = self.dataSource
        self.dataSource.sectionSelectedItem { section, index in
            switch index {
            case 1: // Multi List
                SampleWireframe.performToSampleMultiList(caller: self, data: [:])
            case 2: // Chart
                break
            default: // Display Mode
                break
            }
            
        }
        self.initRefresh(on: self.collectionVw ?? UICollectionView())
        self.observable()
    }

    override func refresh() {
        self.viewModel.loadList()
    }
}

extension SampleListVC {
    
    private func observable() {
        self.viewModel.loadListItem.observe = { [weak self] response in
            guard let self = self else { return }
            self.dataSource.addSampleListItem(items: response)
        }
        
        // Load Is Still Loading
        self.viewModel.loadApi.observe = { [weak self] isLoad in
            guard let self = self else { return }
            HelperVC.loadNetworkActivityIndicator(visible: isLoad)
            HelperVC.showLoading(view: self.view, isLoad: isLoad)
            if !isLoad {
                self.stopRefresh()
            }
        }
        
        // Load Error Message
        self.viewModel.loadError.observe = { message in
            let option: [HelperVC.MessageOptionKey: Any] = [
                .style: SwiftMessages.PresentationStyle.top
            ]
            HelperVC.showMessage(body: message, state: true, option: option)
        }
    }
    
}
