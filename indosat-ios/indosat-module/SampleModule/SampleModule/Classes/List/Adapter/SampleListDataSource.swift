//
//  SampleListDataSource.swift
//  SampleModule
//
//  Created by Irfan Handoko on 23/06/21.
//

import UIKit
import IGListKit
import HelperModule

class SampleListDataSource: NSObject, ListAdapterDataSource {
    /// Section Sample Item
    var sectionList = SampleListSectionController()
    /// Section  Load More
    private var sectionLoadMore = LoadMoreSectionController()
    
    func sectionSelectedItem(onSelectedItem: ((_ section: Int, _ idx: Int)->())?) {
        self.sectionList.onSelectedItem = onSelectedItem
    }
    
    func addSampleListItem(items: [String]) {
        self.sectionList.items = items
        self.sectionList.onRefreshDataSection?()
    }

    func getTotalItemSample() -> Int {
        return self.sectionList.items.count
    }
    
    func getSampleItem(index: Int) -> String {
        return self.sectionList.items[index]
    }
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return ["list", "loadMore"] as [ListDiffable]
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        let type = object as? String
        if type == "list" {
            return self.sectionList
        } else {
            return self.sectionLoadMore
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}


