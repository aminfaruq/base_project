//
//  SampleListSectionController.swift
//  SampleModule
//
//  Created by Irfan Handoko on 23/06/21.
//

import Foundation
import HelperModule

class SampleListSectionController: BaseListSectionController {
    // List Items
    var items: [String] = []
    var onSelectItem: ((_ section: Int, _ idx: Int)->())?
    
    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.minimumInteritemSpacing = 0
        self.minimumLineSpacing = 0
        self.onRefreshDataSection = {
            self.collectionContext?.performBatch(animated: true, updates: { (batchContext) in
                batchContext.reload(self)
            }, completion: nil)
        }
    }
    
    override func numberOfItems() -> Int {
        return items.count
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = SampleListCell.initCell(self.collectionContext, section: self, index: index, bundle: SampleWireframe.bundle, item: items[index]) as! SampleListCell
        cell.configureView()
        cell.setButtonHandler { (view) in
            self.onSelectedItem?(view.tag, index)
        }
        return cell
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return SampleListCell.cellSize(screenWidth: self.collectionContext?.containerSize.width ?? 0, item: items[index])
    }
}


