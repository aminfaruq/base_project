//
//  SampleListVM.swift
//  SampleModule
//
//  Created by Irfan Handoko on 23/06/21.
//

import Foundation
import HelperModule
import NetworkModule

class SampleListVM: BaseVM {
    
    public var loadListItem = Observable<[String]>()
    
    public func loadList() {
        self.loadApi.property = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loadApi.property = false
            self.loadListItem.property = ["Display Model", "Multi List", "Chart"]
        }
    }
}
