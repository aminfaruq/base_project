//
//  SampleListCell.swift
//  SampleModule
//
//  Created by Irfan Handoko on 23/06/21.
//

import UIKit
import HelperModule

class SampleListCell: BaseCollectionCell {
    
    @IBOutlet weak var itemLbl: UILabel?
    @IBOutlet weak var selectBtn: CustomButton?
    
    override var item: Any? {
        didSet {
            guard let item = item as? String else { return }
            self.itemLbl?.text = item
        }
    }
    
    override func setButtonHandler(handler: ((UIView) -> ())?) {
        self.selectBtn?.addHandlerButton(handler: handler)
    }
    
    func configureView() {
        self.selectBtn?.applyStyleBackground(backgroundColor: .clear)
        self.selectBtn?.applyStyleBorder(shadow: 0)
    }
    
    override class func cellSize(screenWidth: CGFloat, item: Any? = nil) -> CGSize {
        let item = item as? String
        let padding32 = CGFloat(32)
        let padding20 = CGFloat(20)
        let padding16 = CGFloat(16)
        let height = TextSize.size(item ?? "", font: UIFont.systemFont(ofSize: 16), width: screenWidth - padding32 - padding20 - padding16, numberOfLines: 0).height
        return CGSize(width: screenWidth, height: height + padding32)
    }
}
