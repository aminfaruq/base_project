Pod::Spec.new do |s|
    s.name         = "SampleModule"
    s.version      = "0.0.1"
    s.summary      = "This is an bank module"
    s.homepage     = "https://gitlab.com/aminfaruq/base_project"
    s.license      = "MIT (ios)"
    s.author             = { "Amin Faruq" => "aminfaruk.fa@gmail.com" }
    s.source       = { :git => "https://gitlab.com/aminfaruq/base_project.git", :tag => "#{s.version}" }
    s.source_files  = "SampleModule/Classes/**/*.{h,m,swift,a}"
    s.resource_bundles = {'SampleModule' => ['SampleModule/Assets/**/*.{storyboard,xib,xcassets,json,imageset,png,bundle}']}
    s.platform         = :ios, "10.0"
    s.dependency 'HelperModule'
    s.dependency 'NetworkModule'
    
end
