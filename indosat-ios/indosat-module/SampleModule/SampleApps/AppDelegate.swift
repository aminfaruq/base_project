//
//  AppDelegate.swift
//  SampleApps
//
//  Created by Irfan Handoko on 24/06/21.
//

import UIKit
import SampleModule
import HelperModule

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = HelperVC.initRootViewController(vc: SampleWireframe.initToSample())
        return true
    }


}

