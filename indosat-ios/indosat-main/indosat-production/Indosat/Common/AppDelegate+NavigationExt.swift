//
//  AppDelegate+NavigationExt.swift
//  Lotus
//
//  Created by Irfan Handoko on 22/06/21.
//

import HelperModule
import NetworkModule
import NavigationModule

extension AppDelegate: NavigationOutputDelegate {
    
    func configNavigation() {
        let navigationWireframe = NavigationWireframe()
        navigationWireframe.output = self
        NavigationManager.shared.navDelegate = navigationWireframe
    }
    
    func configRootNavigation() {
        self.window = HelperVC.initRootViewController(vc: NavigationManager.shared.navDelegate?.initToSample() ?? UIViewController())
    }
    
}
