//
//  AppDelegate.swift
//  Lotus
//
//  Created by Irfan Handoko on 22/06/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Setup Navigation
        self.configNavigation()
        self.configRootNavigation()
        
        return true
    }


}

